## ## ## Quality-Automation-script

##Execute Salesforce-UI-Automation:
### ### Run TestSuite on different environment (Stage, Prod and UAT)
#### Default it's running in stags
### Running on Stage
mvn clean verify
### Running on Prod
mvn clean verify -Denvironment=prod
### Running on UAT
mvn clean verify -Denvironment=uat
### ### Run On Cross Browser
mvn clean verify -DsuiteFile="cross_browser"
### Default its running on Chrome Browser
mvn clean verify
### Run On specific Browser
mvn clean verify -Dbrowser=edge
