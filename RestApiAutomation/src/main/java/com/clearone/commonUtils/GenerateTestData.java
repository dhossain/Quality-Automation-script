package com.clearone.commonUtils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Map;

public class GenerateTestData {

    private static final Logger logger = LogManager.getLogger(GenerateTestData.class);
    private static JSONParser jsonParser = new JSONParser();

    public static JSONObject createLeadJson(String jsonString,
                                            String firstName,
                                            String lastName,
                                            String phoneNumber,
                                            String email,
                                            String ownerId) throws ParseException {
        JSONObject object = (JSONObject) jsonParser.parse(jsonString);
        Object jsonValue = JSONValue.parse(object.toString());
        JSONObject jsonObject = (JSONObject) jsonValue;
        jsonObject.remove("FirstName");
        jsonObject.put("FirstName", firstName);
        jsonObject.remove("LastName");
        jsonObject.put("LastName", lastName);
        jsonObject.remove("Phone");
        jsonObject.put("Phone", phoneNumber.replaceAll("[-.() ]", ""));
        jsonObject.remove("Email");
        jsonObject.put("Email", email);
        jsonObject.remove("SalesAgent");
        jsonObject.put("SalesAgent", ownerId);

        logger.info("FirstName: " + firstName);
        logger.info("LastName: " + lastName);
        logger.info("Phone Number: " + phoneNumber);
        logger.info("Email: " + email);
        logger.info("Request Json Body: " + jsonObject);
        return jsonObject;
    }

    public static JSONObject createLeadJson(String jsonString,
                                            String leadId,
                                            double cPDebtAmt,
                                            String ownerId,
                                            String firstName,
                                            String lastName
                                            ) throws ParseException {
        String email = Utils.getEmail(firstName, lastName);
        String json =
                createLeadJson(
                        jsonString,
                        firstName,
                        lastName,
                        Utils.getPhoneNumber(),
                        email,
                        ownerId).toJSONString();
        JSONObject object = (JSONObject) jsonParser.parse(json);
        Object jsonValue = JSONValue.parse(object.toString());
        JSONObject jsonObject = (JSONObject) jsonValue;
        jsonObject.remove("LeadId");
        jsonObject.put("LeadId", leadId);
        jsonObject.remove("CPDebtAmt");
        jsonObject.put("CPDebtAmt", cPDebtAmt);
        logger.info("Lead ID: " + leadId);
        logger.info("CPDebtAmt: " + cPDebtAmt);
        return jsonObject;
    }

    public static JSONObject createLeadJson(
            String jsonString,
            String firstName,
            String lastName,
            String leadId,
            double cPDebtAmt,
            String ownerId) throws ParseException {
        String email = Utils.getEmail(firstName, lastName);
        String jsonStr =
                createLeadJson(
                        jsonString,
                        firstName,
                        lastName,
                        Utils.getPhoneNumber(),
                        email,
                        ownerId).toString();
        return createLeadJson(jsonStr, leadId, cPDebtAmt, ownerId, firstName, lastName);
    }

    public static JSONObject createLeadJson(
            String originalJsonString,
            String firstName,
            String lastName,
            String birthDate,
            String leadId,
            String cPDebtAmt,
            String ownerId,
            String address,
            String city,
            String zipcode,
            String state,
            String phoneNumber) throws ParseException {

        Map<String, String> newJsonString = new HashMap<>();
        newJsonString.put("Address", address);
        newJsonString.put("City", city);
        newJsonString.put("Zip", zipcode);
        newJsonString.put("State", state);
        newJsonString.put("Debt", cPDebtAmt);
        newJsonString.put("FirstName", firstName);
        newJsonString.put("LastName", lastName);
        newJsonString.put("SalesAgent", ownerId);
        newJsonString.put("DateOfBirth", birthDate);
        newJsonString.put("Email", Utils.getEmail(firstName, lastName));
        newJsonString.put("Phone", phoneNumber.replaceAll("[-.() ]", ""));
        newJsonString.put("LeadId", leadId);
        return Utils.replaceJsonValue(originalJsonString, newJsonString);
    }
}
