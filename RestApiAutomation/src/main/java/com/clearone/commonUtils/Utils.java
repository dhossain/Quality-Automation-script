package com.clearone.commonUtils;
import com.github.javafaker.Faker;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class Utils {

    private static JSONParser jsonParser = new JSONParser();
    private static Faker faker = new Faker();

    /**
     * This function will read file under resources and return as String
     * @param filename
     * @return Data in file as String
     * @throws URISyntaxException
     * @throws IOException
     */
    public static String readFileFromResources(String filename) throws URISyntaxException, IOException {
        URL resource = Utils.class.getClassLoader().getResource(filename);
        byte[] bytes = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(bytes);
    }

    /**
     * This function will read the properties file.
     * @param fileName
     * @return
     * @throws IOException
     */
    public static Properties readPropertiesFile(String fileName) throws IOException {
        InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream(fileName);
        Properties prop = null;
        try {
            prop = new Properties();
            prop.load(inputStream);
        } catch(FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } finally {
            inputStream.close();
        }
        return prop;
    }

    public static JSONObject replaceJsonValue(String jsonString, Map<String, String> jsonKeyValue) throws ParseException {
        JSONObject object = (JSONObject) jsonParser.parse(jsonString);
        Object jsonValue = JSONValue.parse(object.toString());
        JSONObject jsonObject = (JSONObject) jsonValue;
        for(int index = 0; index < jsonKeyValue.size(); index ++){
            jsonObject.remove(jsonKeyValue.keySet().toArray()[index]);
            jsonObject.put(jsonKeyValue.keySet().toArray()[index], jsonKeyValue.values().toArray()[index]);
        }
        return jsonObject;
    }

    public static Object getValueFromCreateLeadResponse(String jsonString, String key) throws ParseException {
        JSONObject object = (JSONObject) jsonParser.parse(jsonString);
        Object jsonValue = JSONValue.parse(object.toString());
        JSONObject jsonObject = (JSONObject) jsonValue;
        return jsonObject.get(key);
    }

    public static String getFirstName() {
        return faker.name().firstName();
    }

    public static String getLastName() {
        return faker.name().lastName();
    }

    public static String getStreetAddress() {
        return faker.address().streetAddress();
    }

    public static String getPhoneNumber() {
        return faker.phoneNumber().cellPhone();
    }

    public static String getEmail(String firstName, String lastName) {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(firstName.charAt(0)).toString()
                .concat(lastName).concat("_")
                .concat(Long.toString(System.currentTimeMillis())
                .concat("@gmail.com")).toLowerCase();
    }
}
