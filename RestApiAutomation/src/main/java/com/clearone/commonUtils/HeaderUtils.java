package com.clearone.commonUtils;

import java.util.HashMap;
import java.util.Map;

public class HeaderUtils {

    public Map<String, String> defaultHeaders(String apiKey) {
        Map <String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put(Headers.APIKEY.name(), apiKey);
        return headers;
    }
}
