package com.clearone.commonUtils;

import com.github.javafaker.Faker;

public class JavaFaker {
    private static Faker faker = new Faker();

    public static String getFirstName() { return faker.name().firstName(); }

    public static String getLastName() { return faker.name().lastName(); }

    public static String getBirthDay() { return faker.date().birthday().toString(); }

    public static String getStreetAddress() { return faker.address().streetAddress(); }

    public static String getPhoneNumber() { return faker.phoneNumber().cellPhone(); }

    public static String getZipCode() { return faker.address().zipCode(); }

    public static String getState() { return faker.address().stateAbbr(); }

    public static String getCity() { return faker.address().cityName(); }

    public static String getEmail(String firstName, String lastName) {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(firstName.charAt(0)).toString()
                .concat(lastName).concat("_")
                .concat(Long.toString(System.currentTimeMillis())
                        .concat("@gmail.com")).toLowerCase();
    }
}
