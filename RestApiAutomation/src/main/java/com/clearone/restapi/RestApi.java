package com.clearone.restapi;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;


public class RestApi {
    private final int CONNECTION_WAIT_TIME = 10*1000;
    private final int READ_WAIT_TIME = 10*1000;

    protected ResponseEntity<String> createRestTemplateRequest(String jsonString, String url, HttpHeaders httpHeaders, HttpMethod methodType){
        HttpEntity<String> entity = new HttpEntity<>(jsonString, httpHeaders);
        return getRestTemplate().exchange(url, methodType, entity, String.class);
    }

    protected RequestSpecification createRestAssuredRequest(Map<String, String> header, String body) {
        return RestAssured.given()
                .when()
                .headers(header)
                .body(body)
                .when();
    }

    protected RequestSpecification createRestAssuredRequest(Map<String, String> header) {
        return RestAssured.given()
                .when()
                .headers(header)
                .when();
    }

    private RestTemplate getRestTemplate() {
        try{
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null, null);
            CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLContext(context).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
            requestFactory.setConnectTimeout(CONNECTION_WAIT_TIME);
            requestFactory.setReadTimeout(READ_WAIT_TIME);
            return new RestTemplate(requestFactory);
        } catch (NoSuchAlgorithmException
                | KeyManagementException e) {
            e.printStackTrace();
            return null;
        }
    }
}
