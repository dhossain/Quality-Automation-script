package com.clearone.restapi;

import io.restassured.response.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class RestApiImpl extends RestApi implements RestApiCall{
    @Override
    public ResponseEntity<String> sendPostRequest(String jsonString, String url, HttpHeaders httpHeaders) {
        return createRestTemplateRequest(jsonString, url, httpHeaders, HttpMethod.POST);
    }

    @Override
    public Response sendPostRequest(String endpoint, Map<String, String> headers, String jsonBody) {
        return createRestAssuredRequest(headers, jsonBody)
                .post(endpoint);
    }

    @Override
    public Response sendGetRequest(Map<String, String> headers, Map<String, Object> parameters, String endpoint) {
        return createRestAssuredRequest(headers)
                .pathParams(parameters)
                .get(endpoint);
    }
}
