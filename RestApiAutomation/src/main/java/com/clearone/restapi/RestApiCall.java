package com.clearone.restapi;

import io.restassured.response.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 *
 */
public interface RestApiCall {

    ResponseEntity<String> sendPostRequest(String jsonString, String url, HttpHeaders httpHeaders);

    Response sendPostRequest(String endpoint, Map<String, String> headers, String jsonBody);

    Response sendGetRequest(Map<String, String> headers, Map<String, Object> parameters, String endpoint);
}
