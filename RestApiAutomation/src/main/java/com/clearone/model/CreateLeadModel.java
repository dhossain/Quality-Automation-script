package com.clearone.model;

import com.clearone.commonUtils.HeaderUtils;
import com.clearone.commonUtils.Headers;
import com.clearone.restapi.RestApiCall;
import com.clearone.restapi.RestApiImpl;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class CreateLeadModel extends ApiProperty {
    private static final Logger logger = LogManager.getLogger(CreateLeadModel.class);
    public RestApiCall restApiCall = new RestApiImpl();
    private HeaderUtils headerUtils = new HeaderUtils();
    private HttpHeaders httpHeaders = null;

    public ResponseEntity createLead(String creatLeadUrl, String jsonBody) {
        try {
            httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            Map<String, String> map = new HashMap<>();
            map.put(Headers.APIKEY.name(), super.CREATE_LEAD_API_KEY);
            httpHeaders.setAll(map);

            ResponseEntity response = restApiCall.sendPostRequest(
                    jsonBody,
                    creatLeadUrl,
                    httpHeaders);
            logger.info("Response Body: " + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Response createLeadInRestAssured(String creatLeadUrl, String jsonBody) {
        return restApiCall.sendPostRequest(creatLeadUrl,
                headerUtils.defaultHeaders(CREATE_LEAD_API_KEY),
                jsonBody);
    }

    public Response getLeadInfo(String endpoint, Map<String, Object> parameters) {
        return restApiCall.sendGetRequest(
                headerUtils.defaultHeaders(GET_LEAD_API_KEY),
                parameters,
                endpoint);
    }
}
