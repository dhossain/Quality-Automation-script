package com.clearone.model;

import com.clearone.commonUtils.Utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class ApiProperty {
    private static Properties getProp = null;
    public static String CREATE_LEAD_URL = null;
    public static String GET_LEAD_URL = null;
    public static String CREATE_LEAD_API_KEY = null;
    public static String GET_LEAD_API_KEY = null;
    public static String CREATE_LEAD_JSON = null;

    public ApiProperty() {
        try {
            this.getProp = Utils.readPropertiesFile("application.properties");
            CREATE_LEAD_URL = getProp.getProperty("creatLeadUrl");
            GET_LEAD_URL = getProp.getProperty("getLeadInfo");
            CREATE_LEAD_API_KEY = getProp.getProperty("APIKEY");
            GET_LEAD_API_KEY = getProp.getProperty("getLeadApiKey");
            CREATE_LEAD_JSON = Utils.readFileFromResources("createLead.json");

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
