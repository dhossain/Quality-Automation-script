package com.clearone.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.junit.jupiter.api.extension.*;

public class ExtentHtmlReport extends TestUtils implements BeforeAllCallback,
        BeforeTestExecutionCallback,
        AfterAllCallback,
        AfterTestExecutionCallback {
    @Override
    public void afterAll(ExtensionContext extensionContext) {
        extentReports.flush();
    }

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) {
        if (!extensionContext.getExecutionException().isPresent()) {
            extentLog.log(Status.PASS, "PASSED");
        } else {
            extentLog.log(Status.FAIL, extensionContext.getExecutionException().get());
        }
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        String path = System.getProperty("user.dir").concat("/target/")
                + "HTML-Report/"
                + extensionContext.getDisplayName()
                +".html";
        ExtentHtmlReporter extentHtmlReport = new ExtentHtmlReporter(path);
        extentReports = new ExtentReports();
        extentReports.attachReporter(extentHtmlReport);
    }

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) {
        extentLog = extentReports.createTest(extensionContext.getDisplayName());
    }
}
