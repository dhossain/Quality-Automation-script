package com.clearone.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.clearone.model.ApiProperty;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;

public class TestUtils extends ApiProperty {
    private static final Logger logger = LogManager.getLogger(TestUtils.class);
    public static ExtentReports extentReports;
    public static ExtentTest extentLog;

    @BeforeEach
    public void setUp(TestInfo testInfo) { logger.info("+++++ START TEST: " + testInfo.getDisplayName() + " +++++"); }

    @AfterEach
    public void tearDown(TestInfo testInfo) { logger.info("+++++ END TEST: " + testInfo.getDisplayName() + " +++++\n"); }
}
