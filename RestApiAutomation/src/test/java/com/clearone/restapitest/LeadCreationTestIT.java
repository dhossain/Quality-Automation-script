package com.clearone.restapitest;

import com.clearone.commonUtils.GenerateTestData;
import com.clearone.commonUtils.JavaFaker;
import com.clearone.commonUtils.Utils;
import com.clearone.model.ApiProperty;
import com.clearone.model.CreateLeadModel;
import com.clearone.utils.ExtentHtmlReport;
import com.clearone.utils.TestUtils;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.Map;

@ExtendWith(ExtentHtmlReport.class)
@SpringBootTest
class LeadCreationTestIT extends TestUtils {

    private CreateLeadModel createLeadModel = new CreateLeadModel();

    @DisplayName("Lead Create")
    @Disabled
    @Test
    void createLead(){
        try {
            String creatLeadUrl = super.CREATE_LEAD_URL;
            String firstName = Utils.getFirstName();
            String lastName = Utils.getLastName();
            extentLog.info("Sending request to: " + creatLeadUrl);
            String jsonBody = GenerateTestData.createLeadJson(
                    CREATE_LEAD_JSON,
                    firstName,
                    lastName,
                    Utils.getPhoneNumber(),
                    Utils.getEmail(firstName, lastName),
                    "0054x000004t6xRAAQ").toString();
            extentLog.info("Request Body: " + jsonBody);
            ResponseEntity response = createLeadModel.createLead(creatLeadUrl, jsonBody);

            extentLog.info("Response Body: " + response.getBody());
            extentLog.info("Status Code: " + response.getStatusCodeValue());
            Assertions.assertEquals(200, response.getStatusCodeValue(), "Status code is NOT 200");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assertions.fail();
        }
    }

    /**
     * In the CsvSource Tag (254 => leadId & 160.12 => cPDebtAmt)
     * @param leadId
     * @param cPDebtAmt
     */
    @DisplayName("Lead Create With LeadIds")
    @Disabled
    @ParameterizedTest(name = "{index} => leadId={0}, cPDebtAmt={1}")
    @CsvSource({
            "254, 10000.12",
            "255, 0",
    })
    public void leadCreateWithLeadId(String leadId, String cPDebtAmt) {
        try {
            String firstName = Utils.getFirstName();
            String lastName = Utils.getLastName();
            String creatLeadUrl = super.CREATE_LEAD_URL;
            extentLog.info("Sending request to: " + creatLeadUrl);
            String jsonBody = GenerateTestData.createLeadJson(
                    CREATE_LEAD_JSON,
                    leadId,
                    Double.parseDouble(cPDebtAmt),
                    "0054x000004t6xRAAQ",
                    firstName,
                    lastName).toString();
            extentLog.info("Request Body: " + jsonBody);
            ResponseEntity response = createLeadModel.createLead(creatLeadUrl, jsonBody);

            extentLog.info("Response Body: " + response.getBody());
            extentLog.info("Status Code: " + response.getStatusCodeValue());
            Assertions.assertEquals(200, response.getStatusCodeValue(), "Status code is NOT 200");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assertions.fail();
        }
    }

    @DisplayName("Lead Create Using Rest-Assured")
    @Test
    void createLeadUsingRestAssured(){
        try {
            String creatLeadUrl = super.CREATE_LEAD_URL;
            extentLog.info("Sending request to: " + creatLeadUrl);
            String jsonBody = GenerateTestData.createLeadJson(
                    CREATE_LEAD_JSON,
                    JavaFaker.getFirstName(),
                    JavaFaker.getLastName(),
                    "01/01/1998",
                    "9999",
                    "200",
                    "0054x000004t6xRAAQ",
                    JavaFaker.getStreetAddress(),
                    JavaFaker.getCity(),
                    JavaFaker.getZipCode(),
                    JavaFaker.getState(),
                    JavaFaker.getPhoneNumber()).toString();
            extentLog.info("Request Body: " + jsonBody);

            Response response = createLeadModel.createLeadInRestAssured(creatLeadUrl, jsonBody);

            extentLog.info("Response Body: " + response.getBody().asString());
            extentLog.info("Status Code: " + response.statusCode());
            Assertions.assertEquals(200, response.statusCode(), "Status code is NOT 200");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assertions.fail();
        }
    }

    @DisplayName("Get Lead Information Using Rest-Assured")
    @Disabled
    @Test
    public void getLead() {
        try{
            Response response = createLeadModel.getLeadInfo(ApiProperty.GET_LEAD_URL, Map.of("phoneNumber", "12682236085"));
            Assertions.assertEquals(200, response.statusCode());
            extentLog.info("Get Lead Response Body: " + response.getBody().asString());
        } catch (Exception ex) {
            ex.printStackTrace();
            Assertions.fail();
        }

    }
}
