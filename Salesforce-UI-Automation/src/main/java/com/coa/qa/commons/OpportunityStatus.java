package com.coa.qa.commons;

public enum OpportunityStatus {
    Final_Approval("Final Approval"),
    Contract_Signed("Contract Signed");

    public final String VALUE;
    OpportunityStatus(String value) {
        this.VALUE = value;
    }
}
