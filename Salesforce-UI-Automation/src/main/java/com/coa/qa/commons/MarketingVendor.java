package com.coa.qa.commons;

public enum MarketingVendor {
    TestMarketingVendor("TestMarketing Vendor"),
    TestMarketingVendorId("9999");

    public final String VALUE;
    MarketingVendor(String value) {
        this.VALUE = value;
    }
}
