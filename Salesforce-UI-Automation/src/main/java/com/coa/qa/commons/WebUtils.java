package com.coa.qa.commons;

import com.coa.qa.actions.ExpectedCondition;
import com.coa.qa.actions.GetWebElement;
import com.coa.qa.actions.Selectors;
import com.coa.qa.helper.BaseClass;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebUtils extends BaseClass {
    private static final Logger logger = LogManager.getLogger(WebUtils.class);
    private static GetWebElement getWebElement = new GetWebElement();

    private static final int IMPLICITLY_WAIT_TIME_SECOND = 60;
    private static final int EXPLICIT_WAIT_TIME_SECOND = 60;
    private static final int ELEMENT_IS_PRESENT_WAIT_TIME_SECOND = 3;

    public static void implicitlyWait() {
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIME_SECOND, TimeUnit.SECONDS);
    }

    public static void explicitWait(int duration, By element) {
        WebDriverWait wait = new WebDriverWait(driver, duration);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        logger.info("Explicit Wait: " + duration);
    }

    private static WebElement explicitWait(By element, ExpectedCondition conditions) {
        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECOND);
        WebElement elmt = null;

        try {
            switch (conditions) {
                case elementIsVisible:
                    elmt = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
                    break;
                case elementToBeClickable:
                    elmt = wait.until(ExpectedConditions.elementToBeClickable(element));
                    break;
                default:
                    throw new Exception("Element Not Found");
            }
            return elmt;
        } catch (Exception ex) {
            ex.getMessage();
            return null;
        }
    }

    public static List<WebElement> waitElement(String elements, Selectors selectors) throws Exception {
        List<WebElement> element = getWebElement.getElements(elements, selectors);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_IS_PRESENT_WAIT_TIME_SECOND);
        wait.until(new Function<WebDriver, List<WebElement>>() {
            @Override
            public List <WebElement> apply(WebDriver driver) {
                return element;
            }
        });
        return element;
    }

    public static WebElement explicitWait(String element, Selectors selectors, ExpectedCondition expectedCondition) {
        try {
            By elementUsingBy = getWebElement.getElementUsingBy(element, selectors);
            WebUtils.explicitWait(elementUsingBy, expectedCondition);
            return driver.findElement(elementUsingBy);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<WebElement> explicitWaitElements(String element, Selectors selectors, ExpectedCondition expectedCondition) {
        try {
            By elementUsingBy = getWebElement.getElementUsingBy(element, selectors);
            WebUtils.explicitWait(elementUsingBy, expectedCondition);
            return driver.findElements(elementUsingBy);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String getLeadIdFromUrl(String url) {
        String urlSplit[] = url.split("/");
        String leadId = urlSplit[urlSplit.length - 2];
        logger.info("Lead Id: " + leadId);
        return leadId;
    }

    public static String getLeadIdFromUrl() throws InterruptedException {
        String urlSplit[] = getCurrentUrl().split("/");
        String leadId = urlSplit[urlSplit.length - 2];
        logger.info("Lead Id: " + leadId);
        return leadId;
    }

    public static String getCurrentUrl() throws InterruptedException {
        String url = driver.getCurrentUrl();
        logger.info("Current Url: " + url);
        return url;
    }

    public static void enterUrl(String url) {
        ((JavascriptExecutor)driver).executeScript("window.location = \'"+url+"\'");
        logger.info("Enter URL: " + url);
    }

    public static String getLeadUrl(String leadId) {
        String baseUrl = config.getProperty("homeUrl");
        String parameter = "r/Lead/".concat(leadId).concat("/view");
        String leadUrl = baseUrl.replace("page/home", parameter);
        logger.info("Lead URL: " + leadUrl);
        return leadUrl;
    }

    public static String getOpportunityUrl(String leadId) {
        String baseUrl = config.getProperty("homeUrl");
        String parameter = "r/Opportunity/".concat(leadId).concat("/view");
        String opportunityUrl = baseUrl.replace("page/home", parameter);
        logger.info("Opportunity URL: " + opportunityUrl);
        return opportunityUrl;
    }

    public static void threadWait(int timeInSecond) {
        try{
            Thread.sleep(timeInSecond * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void pageReload() {
        try {
            logger.info("Re-Loading Current Page");
            driver.get(getCurrentUrl());
        } catch (InterruptedException e) {
            logger.error("Exception: ", e);
            e.printStackTrace();
        }
    }
}
