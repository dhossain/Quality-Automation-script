package com.coa.qa.commons;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class Utils {

    /**
     * This method generate random number in the base on length,
     * And return it as String
     * @param length
     * @return String random number
     */
    public static String randomNumber(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) +  '1');
        for (int i = 1; i < length; i ++) {
            digits[i] = (char) (random.nextInt(10) +  '0');
        }
        return new String(digits);
    }

    public static String[] splitString(String string, String regularEx) { return string.split(regularEx); }
    /**
     * This method generate random number base on minimum and maximum number,
     * And return it as String
     * @param minimum
     * @param maximum
     * @return String random number
     */
    public static String randomNumber(int minimum, int maximum) {
        int randNumber = (int) Math.floor(Math.random() * (maximum - minimum + 1) + minimum);
        return String.valueOf(randNumber);
    }

    public static String futureDate(int howMuchFuture) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date()); // Using today's date
        calendar.add(Calendar.DATE, howMuchFuture); // Adding future days
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String setDate(long pastYear) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.now().minusYears(pastYear);
        return localDate.format(formatter);
    }

    public static String getEpochMilliSeconds() {
        Date date = new Date();
        String millis = String.valueOf(date.getTime());
        return millis;
    }

    public static String getDigitsFromEpochSeconds(int n) {
        Date date = new Date();
        String secs = String.valueOf(date.getTime() / 1000);
        return secs.substring(secs.length() - n);
    }

    public static String getDigitsFromEpochMilliseconds(int n) {
        Date date = new Date();
        String millis = String.valueOf(date.getTime());
        return millis.substring(millis.length() - n);
    }

    public static String getCurrentTimeMySQL() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }

    public static String getTimeMidnightYesterday() {
        LocalDate tomorrow = LocalDate.now().minusDays(1);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }

    public static String getTimeMidnightinPast(int days) {
        LocalDate tomorrow = LocalDate.now().minusDays(days);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }

    public static String getTimeMidnight() {
        LocalDate tomorrow = LocalDate.now();
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }

    public static String getCurrentDateOnly() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dt);
    }

    public static String getDateInFormat(String format) {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String getFutureDateInFormat(String format, int days) {
        LocalDate tomorrow = LocalDate.now().plusDays(days);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String addDaysTo(String oldDate, String format, int days){
        String[] old = oldDate.split("/", 5);
        LocalDate tomorrow = LocalDate.of(Integer.parseInt(old[2]), Integer.parseInt(old[0]),Integer.parseInt(old[1])).plusDays(days);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String addMonthsTo(String oldDate, String format, int months){
        String[] old = oldDate.split("/", 5);
        LocalDate tomorrow = LocalDate.of(Integer.parseInt(old[2]), Integer.parseInt(old[0]),Integer.parseInt(old[1])).plusMonths(months);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String addYearsTo(String oldDate, String format, int years){
        String[] old = oldDate.split("/", 5);
        LocalDate tomorrow = LocalDate.of(Integer.parseInt(old[2]), Integer.parseInt(old[0]),Integer.parseInt(old[1])).plusYears(years);
        Date dt = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String getSameDateInFutureMonths(String format, int months) {
        LocalDate tom = LocalDate.now().plusMonths(months);
        Date dt = Date.from(tom.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static String getSameDateInFutureYears(String format, int years) {
        LocalDate tom = LocalDate.now().plusYears(years);
        Date dt = Date.from(tom.atStartOfDay(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }

    public static int getCurrentYear(){
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static String getDateOnlyFor(Date dt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dt);
    }

    public static String getCurrentTimeEST() {
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4:00"));
        return sdf.format(dt);
    }

//    public static void CompareTwoString(String expectedValue, String actualValue, boolean testCase) throws FailedTestCases {
//        if (testCase) {
//            if (expectedValue.contains(actualValue)) {
//                System.out.println("Actual Text '" + expectedValue + "' matches with Expected Text '" + actualValue + "'");
//
//            } else {
//                System.out.println("Actual Text '" + expectedValue + "' Doesn't matches with Expected Text '" + actualValue + "'");
//                throw new FailedTestCases("Text Are Not Equals!!!/n"
//                        + "Actual Text " + expectedValue + "Doesn't matches with Expected Text " + actualValue);
//
//            }
//        }
//    }
}
