package com.coa.qa.commons;

public enum RelatedCreditor {
    VillasAtChase("Villas at Chase"),
    ChaseBank("Chase Bank");

    public final String VALUE;
    RelatedCreditor(String value) {
        this.VALUE = value;
    }
}
