package com.coa.qa.commons;

public enum BankingInfo {
    BankName("Chase Bank"),
    ChaseBank_RoutingNumber("044000037");

    public final String VALUE;
    BankingInfo(String value) {
        this.VALUE = value;
    }
}
