package com.coa.qa.commons;

import com.clearone.commonUtils.GenerateTestData;
import com.clearone.commonUtils.JavaFaker;
import com.clearone.commonUtils.Utils;
import com.coa.qa.restapi.ApiProperties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;

public class ApiUtils extends ApiProperties {
    private static final Logger logger = LogManager.getLogger(ApiUtils.class);

    public String getLeadUrl(Object responseBody) throws ParseException {
        return WebUtils.getLeadUrl(Utils.getValueFromCreateLeadResponse(
                        responseBody.toString(), "Data")
                .toString());
    }

    public String setRequestBody(String firstName, String lastName, double cPDebtAmount) throws ParseException {
        return GenerateTestData.createLeadJson(
                super.CREATE_LEAD_JSON,
                firstName,
                lastName,
                MarketingVendor.TestMarketingVendorId.VALUE,
                cPDebtAmount,
                config.getProperty("salesAgentId")).toString();
    }

    public String setLeadCreateRequestBody(String firstName, String lastName, double cPDebtAmount, String agentId, String leadId) throws ParseException {
        return GenerateTestData.createLeadJson(
                super.CREATE_LEAD_JSON,
                firstName,
                lastName,
                leadId,
                cPDebtAmount,
                agentId).toString();
    }

    public String setLeadCreateRequestBody(String firstName, String lastName, String cPDebtAmount, String state) throws ParseException {
        return GenerateTestData.createLeadJson(
                super.CREATE_LEAD_JSON,
                firstName,
                lastName,
                com.coa.qa.commons.Utils.setDate(25),
                MarketingVendor.TestMarketingVendorId.VALUE,
                cPDebtAmount,
                config.getProperty("salesAgentId"),
                JavaFaker.getStreetAddress(),
                JavaFaker.getCity(),
                JavaFaker.getZipCode(),
                state,
                JavaFaker.getPhoneNumber()).toString();
    }
}
