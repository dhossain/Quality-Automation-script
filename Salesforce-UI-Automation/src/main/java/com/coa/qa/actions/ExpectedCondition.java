package com.coa.qa.actions;

public enum ExpectedCondition {
    elementToBeSelected,
    elementIsVisible,
    elementToBeClickable,
    textToBePresentInElement,
    visibilityOfElementLocated,
    alertIsPresent
}
