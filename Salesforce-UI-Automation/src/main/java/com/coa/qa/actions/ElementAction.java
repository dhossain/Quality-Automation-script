package com.coa.qa.actions;

import com.coa.qa.commons.WebUtils;
import com.coa.qa.helper.BaseClass;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ElementAction extends BaseClass {
    private static final Logger logger = LogManager.getLogger(ElementAction.class);
    private static GetWebElement getWebElement = new GetWebElement();

    public static void clickOn(String element, Selectors selectors){
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            webElement.click();
            logger.info("Clicked on Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element);
        }
    }

    public static void clickOn(WebElement element){
        try {
            element.click();
            logger.info("Clicked on Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element);
        }
    }

    public static void clickWithJavaScript(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            scrollToCenter(webElement);
            JavascriptExecutor js= (JavascriptExecutor)driver;
            js.executeScript("arguments[0].click();", webElement);
            logger.info("Clicked on Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element +" ", e);
        }
    }

    public static void clickWithJavaScriptOnStaleElement(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            scrollToCenter(webElement);
            JavascriptExecutor js= (JavascriptExecutor)driver;
            js.executeScript("arguments[0].click();", webElement);
            logger.info("Clicked on Element: " + element);
        } catch (StaleElementReferenceException se) {
            logger.debug("Stale Element Exception: ", se);
            clickWithJavaScriptOnStaleElement(element, selectors);
        }
    }

    public static void clickWithJavaScript(WebElement element) {
        try {
            scrollToCenter(element);
            JavascriptExecutor js= (JavascriptExecutor)driver;
            js.executeScript("arguments[0].click();", element);
            logger.info("Clicked on Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element +" ", e);
        }
    }

    public static void clickWithKeyBord(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            webElement.sendKeys(Keys.ENTER);
            logger.info("Clicked on Element with KeyBord: " + element);
        } catch (StaleElementReferenceException se) {
            logger.debug("Stale Element Exception: ", se);
            clickWithKeyBord(element, selectors);
        } catch (NoSuchElementException
                | NoSuchWindowException
                | NoSuchFrameException
                | NoAlertPresentException
                | ElementNotVisibleException
                | ElementNotSelectableException
                | TimeoutException
                | NoSuchSessionException ex) {
            logger.error("Not Able to click on WebElement: " + element + " - ", ex);
        }
    }

    public static boolean isElementPresent(String element, Selectors selectors) {
        try {
            List<WebElement> elements = WebUtils.waitElement(element, selectors);
            boolean isPresent = elements.size() > 0;
            if (isPresent) { logger.info("Element is present: " + element); }
            return isPresent;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(element + " - Element is NOT present ", e);
            return false;
        }
    }

    public static void clickWithAction(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            Actions actions = new Actions(driver);
            actions.moveToElement(webElement);
            actions.click(webElement);
            Action action = actions.build();
            action.perform();
            logger.info("Clicked on Element with Action: " + element);
        } catch (StaleElementReferenceException se) {
            logger.debug("Stale Element Exception: ", se);
            clickWithAction(element, selectors);
        } catch (NoSuchElementException
                | NoSuchWindowException
                | NoSuchFrameException
                | NoAlertPresentException
                | ElementNotVisibleException
                | ElementNotSelectableException
                | TimeoutException
                | NoSuchSessionException ex) {
            logger.error("Not Able to click on WebElement: " + element + " - ", ex);
        }
    }

    public static void clickOnWithExplicitWait(String element, Selectors selectors){
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeClickable);
            webElement.click();
            logger.info("Clicked on Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element);
        }
    }

    public static void clickElementForcefully(String element, Selectors selectors) {
        try {
            WebElement webElement = getWebElement.getElement(element, selectors);
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
            webElement.click();
            logger.info("Clicked on Element: " + webElement);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + element);
        }
    }

    public static void clickAfterScrollToView(String element, Selectors selectors) {
        WebElement webElement = null;
        try {
            webElement = getWebElement.getElement(element, selectors);
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
            webElement.click();
            logger.info("Clicked on Element: " + webElement);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to click on the element: " + webElement);
        }
    }

    public static void scrollToCenter(WebElement element) {
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'})", element);
            logger.info("scrollToCenter for the Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to scrollToCenter: " + element);
        }
    }

    public static String getText(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementIsVisible);
            scrollToCenter(webElement);
            drawBorderOnTheElement(webElement);
            String text = webElement.getText();
            logger.info("Get Text: " + text + ", On the element: " + element);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to getting text on the element: " + element);
            return null;
        }
    }

    public static String getText(WebElement element) {
        try {
            scrollToCenter(element);
            drawBorderOnTheElement(element);
            String text = element.getText();
            logger.info("Get Text: " + text + ", On the element: " + element);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to get text: " + element + " - ", e);
            return null;
        }
    }

    public static String getAttributeValue(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementIsVisible);
            scrollToCenter(webElement);
            drawGreenBorderOnTheElement(webElement);
            String text = webElement.getAttribute("value");
            logger.info("Get Attribute Value: " + text + ", on the element: " + element);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to getting text on the element: " + element);
            return null;
        }
    }

    public static void enterText(String element, Selectors selectors, String text) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementIsVisible);
            webElement.clear();
            webElement.sendKeys(text);
            logger.info("On the Element: " + element + ", Text Entered: " + text);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to enter text on the element: " + element);
        }
    }

    public static void clearText(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementIsVisible);
            webElement.clear();
            logger.info("Text clear on the Element: " + element);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to enter text on the element: " + element);
        }
    }

    public static boolean isDisplayed(String element, Selectors selectors) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementIsVisible);
            scrollToCenter(webElement);
            logger.info("Element should be Displayed: " + element);
            return webElement.isDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Element is not displayed: " + element);
            return false;
        }
    }

    public static boolean isDisplayed(WebElement element) {
        try {
            scrollToCenter(element);
            return element.isDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Element is not displayed: " + element);
            return false;
        }
    }

    public static List<WebElement> getWebElements(String elements, Selectors selectors) {
        return WebUtils.explicitWaitElements(elements, selectors, ExpectedCondition.elementIsVisible);
    }

    public static WebElement getElementFromListByText(List<WebElement> element, String elementText) {
        for (WebElement webElement : element) {
            if (webElement.getText().equalsIgnoreCase(elementText))
                return webElement;
        }
        return null;
    }

    /**
     * Using this function we can have an element from a list of WebElement by using its index
     * @param elements - List of WebElement
     * @param selectors
     * @param index - Index of desire element
     * @return
     */
    public static WebElement getElementByIndex(String elements, Selectors selectors, int index) {
        List<WebElement> element = getWebElements(elements, selectors);
        return element.get(index);
    }

    public static void switchFrameTo(int frameNumber) {
        driver.switchTo().frame(frameNumber);
    }

    public static void closeBrowser() {
        driver.close();
        logger.info("Browser is closed !!!");
    }

    public static void selectElementByName(String element, Selectors selectors, String value) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeSelected);
            Select select = new Select(webElement);
            select.selectByValue(value);
            logger.info(element + " Element is selected by - " + value);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to select the element: " + element);
        }

    }

    public static void selectElementByIndex(String element, Selectors selectors, int index) {
        try {
            WebElement webElement = WebUtils.explicitWait(element, selectors, ExpectedCondition.elementToBeSelected);
            Select select = new Select(webElement);
            select.selectByIndex(index);
            logger.info(element + " Element is selected by index - " + index);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to select the element: " + element);
        }
    }
}