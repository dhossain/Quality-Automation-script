package com.coa.qa.actions;

import com.coa.qa.helper.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class GetWebElement extends BaseClass {

    protected WebElement getElement(String element, Selectors selector) throws Exception {
        WebElement webElement = null;
        switch (selector) {
            case id:
                webElement = driver.findElement(By.id(element));
                break;
            case className:
                webElement = driver.findElement(By.className(element));
                break;
            case xpath:
                webElement = driver.findElement(By.xpath(element));
                break;
            case cssSelector:
                webElement = driver.findElement(By.cssSelector(element));
                break;
            case linkText:
                webElement = driver.findElement(By.linkText(element));
                break;
            case name:
                webElement = driver.findElement(By.name(element));
                break;
            case partialLinkText:
                webElement = driver.findElement(By.partialLinkText(element));
                break;
            case tagName:
                webElement = driver.findElement(By.tagName(element));
                break;
            default:
                throw new Exception("Element Not Found");
        }
        return webElement;
    }

    public By getElementUsingBy(String element, Selectors selector) throws Exception {
        By webElement = null;

        switch (selector) {
            case id:
                webElement = By.id(element);
                break;
            case className:
                webElement = By.className(element);
                break;
            case xpath:
                webElement = By.xpath(element);
                break;
            case cssSelector:
                webElement = By.cssSelector(element);
                break;
            case linkText:
                webElement = By.linkText(element);
                break;
            case name:
                webElement = By.name(element);
                break;
            case partialLinkText:
                webElement = By.partialLinkText(element);
                break;
            case tagName:
                webElement = By.tagName(element);
                break;
            default:
                throw new Exception("Element Not Found");
        }
        return webElement;
    }

    public List<WebElement> getElements(String element, Selectors selector) throws Exception {
        List<WebElement> webElements = null;
        switch (selector) {
            case id:
                webElements = driver.findElements(By.id(element));
                break;
            case className:
                webElements = driver.findElements(By.className(element));
                break;
            case xpath:
                webElements = driver.findElements(By.xpath(element));
                break;
            case cssSelector:
                webElements = driver.findElements(By.cssSelector(element));
                break;
            case linkText:
                webElements = driver.findElements(By.linkText(element));
                break;
            case name:
                webElements = driver.findElements(By.name(element));
                break;
            case partialLinkText:
                webElements = driver.findElements(By.partialLinkText(element));
                break;
            case tagName:
                webElements = driver.findElements(By.tagName(element));
                break;
            default:
                throw new Exception("Element Not Found");
        }
        return webElements;
    }
}