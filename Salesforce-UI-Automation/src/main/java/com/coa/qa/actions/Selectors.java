package com.coa.qa.actions;

public enum Selectors {
    id,
    className,
    xpath,
    cssSelector,
    linkText,
    name,
    partialLinkText,
    tagName
}
