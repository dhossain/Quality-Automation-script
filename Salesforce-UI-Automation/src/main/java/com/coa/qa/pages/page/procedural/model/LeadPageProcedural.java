package com.coa.qa.pages.page.procedural.model;

import com.coa.qa.pages.pageobject.CommonActionFlowObject;
import com.coa.qa.pages.pageobject.LeadsPageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

public class LeadPageProcedural {
    private static final Logger logger = LogManager.getLogger(LeadPageProcedural.class);
    private CommonActionFlowObject commonActionFlowObject = new CommonActionFlowObject();
    private LeadsPageObject leadsPageObject = new LeadsPageObject();

    public List<String> getDuplicateLeads() {
        commonActionFlowObject.clickOnTab("Related");
        return leadsPageObject.getListOfDuplicateLeads();
    }

    public List<String> getCreatedTasks() { return leadsPageObject.getListOfCreatedTask(); }

    public String getInContractFileName() { return leadsPageObject.getInContractFileName(); }
}
