package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.ApprovalQueuePageElements;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ApprovalQueuePageObject implements ApprovalQueuePageElements {
    public void clickOnTakeOwnership() {
        ElementAction.clickWithJavaScript(takeOwnership_xpath, Selectors.xpath);
    }

    public void clickOnNextBtn() {
        ElementAction.clickWithJavaScript(nextBtn_xpath, Selectors.xpath);
    }

    public void selectOutcomeRequest(String outcome) {
        List<WebElement> elements = ElementAction.getWebElements(outcomeRequestList_xpath, Selectors.xpath);
        WebElement element = ElementAction.getElementFromListByText(elements, outcome);
        ElementAction.clickWithJavaScript(element);
    }

    public void writeDecisionComments(String comments) { ElementAction.enterText(textCommandArea_xpath, Selectors.xpath, comments); }

    public String getStatus() {
        WebElement element = ElementAction.getElementByIndex(statusAndDate_xpath, Selectors.xpath, 0);
        return ElementAction.getText(element);
    }

    public String getStatusChangeDate() {
        WebElement element = ElementAction.getElementByIndex(statusAndDate_xpath, Selectors.xpath, 1);
        return ElementAction.getText(element);
    }

    public String getMessage() {
        WebElement element = ElementAction.getElementByIndex(approvalRequestWas_xpath, Selectors.xpath, 0);
        return ElementAction.getText(element) + " " + getStatus();
    }

    public void clickOnDetailsTab() { ElementAction.clickOn(detailsTab_xpath, Selectors.xpath); }

    public String getOpportunityName() { return ElementAction.getText(opportunityName_xpath, Selectors.xpath); }

    public String getOppStage() { return ElementAction.getText(oppStage_xpath, Selectors.xpath); }

    public String getCurrentSubStage() { return ElementAction.getText(currentSubStage_xpath, Selectors.xpath); }
}
