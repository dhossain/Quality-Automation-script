package com.coa.qa.pages.webelements;

public interface DraftFrequencyElements {
    String draftFrequency_xpath = "//button[@name = 'Draft_Frequency__c']";
    String draftFrequencyValues_xpath = "//lightning-base-combobox-item/span/span";
    String programStartDate_xpath = "//input[@name = 'Program_Start_Date__c']";
}
