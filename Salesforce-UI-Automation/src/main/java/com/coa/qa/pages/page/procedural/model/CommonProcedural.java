package com.coa.qa.pages.page.procedural.model;

import com.clearone.commonUtils.Utils;
import com.coa.qa.commons.WebUtils;
import com.coa.qa.helper.BaseClass;
import com.coa.qa.pages.pageobject.CommonActionFlowObject;
import com.coa.qa.pages.pageobject.HomePageObject;
import com.coa.qa.pages.pageobject.LeadsPageObject;
import com.coa.qa.pages.pageobject.NavigationPageObject;
import com.coa.qa.testdata.CoaStates;
import com.coa.qa.testdata.Processor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.stream.IntStream;

public class CommonProcedural extends BaseClass implements CoaStates {
    private static final Logger logger = LogManager.getLogger(CommonProcedural.class);
    private HomePageObject homePageObject = new HomePageObject();
    private NavigationPageObject navigationPageObject = new NavigationPageObject();
    private LeadsPageObject leadsPageObject = new LeadsPageObject();
    private CommonActionFlowObject commonActionFlowObject = new CommonActionFlowObject();

    public void logIn(String userName, String password) {
        try {
            logger.info("Login is called");
            homePageObject.enterUserName(userName);
            homePageObject.enterPassword(password);
            homePageObject.clickOnSubmitBtn();
            if (homePageObject.isAdpReminderDisplayed()) {
                homePageObject.clickOnAdpReminderBtn();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to login !!!");
        }
    }

    public void closeTabs() {
        navigationPageObject.closeAllOpenTabs();
    }

    public boolean loggedOut() {
        try {
            logger.info("LogOut method is called");
            closeTabs();
            navigationPageObject.clickOnTheLogOutBtn();
            return WebUtils.getCurrentUrl().equals(config.getProperty("url"));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to LogOut !!!");
            return false;
        }
    }

    public void navigateToLeadPage() {
        try {
            navigationPageObject.clickOnNavigationMenu();
            navigationPageObject.clickOnTheLeadTab();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to land on the Lead Page !!!");
        }
    }

    public boolean managerConversion(boolean isPhoneNumberNeeded) {
        logger.info("Convert a lead using Manager Conversion");
        try {
            leadsPageObject.clickOnNextBtnInManagerConversion();
            if (isPhoneNumberNeeded) {
                leadsPageObject.enterPhoneNumberInManagerConversion(Utils.getPhoneNumber());
            }
            leadsPageObject.clickOnNextBtnInManagerConversion();
            leadsPageObject.selectHardshipManagerConversion();
            leadsPageObject.enterWhoDidInManagerConversion("Test-Clint");
            leadsPageObject.enterWhenDidInManagerConversion("Test-Clint-When");
            leadsPageObject.enterHowMuchDidInManagerConversion("75%");
            leadsPageObject.clickOnConvertBtnLeadInManagerConversion();
            return leadsPageObject.isOpportunityTitleDisplayed();
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Manager Conversion is NOT done !!");
            return false;
        }
    }

    public boolean createLeads(String firstName, String lastName, String email) {
        logger.info("Create Lead with minimum Info");
        try {
            leadsPageObject.enterMinimumCustomerInfo(firstName, lastName, email);
            leadsPageObject.clickOnSaveBtn();
            return leadsPageObject.isCustomerNameDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to create a leads !!!");
            return false;
        }
    }

    public boolean createLeadsWithCoApplicant(String applicantFirstName,
                                              String applicantLastName,
                                              String coApplicantFirstName,
                                              String coApplicantLastName) {
        logger.info("Create Lead with Co-Applicant");
        try {
            String applicantEmail = Utils.getEmail(applicantFirstName,applicantLastName);
            String coApplicantEmail = Utils.getEmail(coApplicantFirstName,coApplicantLastName);
            leadsPageObject.createLeadWithCoApplicant(
                    applicantFirstName,
                    applicantLastName,
                    applicantEmail,
                    coApplicantFirstName,
                    coApplicantLastName,
                    coApplicantEmail,
                    null);
            leadsPageObject.clickOnSaveBtn();
            return leadsPageObject.isCustomerNameDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to create a leads !!!");
            return false;
        }
    }

    public String setLeadOwner(String firstName, String lastName) {
        logger.info("Update Lead Owner Name.");
        try {
            leadsPageObject.clickOnChangeOwnerIcon();
            leadsPageObject.enterOwnerName(firstName, lastName);
            leadsPageObject.selectOwner(firstName, lastName);
            leadsPageObject.clickOnChangeOwnerBtn();
            WebUtils.pageReload();
            return leadsPageObject.getOwnerName();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Lead Owner name is NOT Update!!!");
            return null;
        }
    }

    public String getCoaProcessor(String state) {
        try {
            String getProcessor = null;
            String processor = commonActionFlowObject.getProcessor();
            logger.info("Processor Validation Start - " + processor);
            if (IntStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8).anyMatch(i -> attorneyModelStates[i].equalsIgnoreCase(state))
                    && processor.equalsIgnoreCase(Processor.GREENBERG.VALUE)) {
                getProcessor = processor;
            } else if (IntStream.of(9).anyMatch(i -> attorneyModelStates[i].equalsIgnoreCase(state))
                    && processor.equalsIgnoreCase(Processor.ALPERSTEIN.VALUE)) {
                getProcessor = processor;
            }
            return getProcessor;
        }  catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            return null;
        }
    }

    public boolean callScriptTaskValidation(String state) {
        logger.info("Call Script Task Validation Start");
        try {
            boolean isProcessorValid = false;
            if (IntStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8).anyMatch(i -> attorneyModelStates[i].equalsIgnoreCase(state))) {
                isProcessorValid = commonActionFlowObject.getCallScriptTask().contains(Processor.GREENBERG.VALUE.toUpperCase());
            } else if (IntStream.of(9).anyMatch(i -> attorneyModelStates[i].equalsIgnoreCase(state))) {
                isProcessorValid = commonActionFlowObject.getCallScriptTask().contains(Processor.ALPERSTEIN.VALUE.toUpperCase());
            } else {
                throw new IllegalStateException("Unexpected value: " + state);
            }
            return isProcessorValid;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            return false;
        }
    }
}
