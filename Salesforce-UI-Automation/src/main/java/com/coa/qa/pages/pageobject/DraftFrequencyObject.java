package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.DraftFrequencyElements;
import org.openqa.selenium.WebElement;

public class DraftFrequencyObject implements DraftFrequencyElements {

    public void selectDraftFrequencyMonthly() {
        ElementAction.clickOn(draftFrequency_xpath, Selectors.xpath);
        WebElement monthlyDraftFrequency = ElementAction.getElementByIndex(draftFrequencyValues_xpath, Selectors.xpath, 1);
        ElementAction.clickOn(monthlyDraftFrequency);
    }

    public void enterProgramStartDate(String startDate) {
        ElementAction.enterText(programStartDate_xpath, Selectors.xpath, startDate);
    }
}
