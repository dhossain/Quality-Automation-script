package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.OpportunityActionIncomeExpensesElements;

public class OpportunityActionIncomeExpensesObject implements OpportunityActionIncomeExpensesElements {
    public boolean isApplicantNetSalaryTextDisplayed() {
        return ElementAction.isDisplayed(applicantNetSalaryText_xpath, Selectors.xpath);
    }

    public void enterApplicantNetSalary(String netSalary) {
        ElementAction.enterText(applicantNetSalary_xpath, Selectors.xpath, netSalary);
    }

    public boolean isRentDisplayed() {
        return ElementAction.isDisplayed(applicantRent_xpath, Selectors.xpath);
    }

    public void enterApplicantRent(String rentAmount) {
        ElementAction.enterText(applicantRent_xpath, Selectors.xpath, rentAmount);
    }

    public boolean isHealthInsuranceDisplayed() {
        return ElementAction.isDisplayed(healthInsuranceInput_xpath, Selectors.xpath);
    }

    public boolean isCableInternetDisplayed() {
        return ElementAction.isDisplayed(cableInternetInput_xpath, Selectors.xpath);
    }

    public boolean isMarketValuePrimaryHouseDisplayed() {
        return ElementAction.isDisplayed(marketValuePrimaryHouseInput_xpath, Selectors.xpath);
    }

    public boolean isPrimaryHomeMortgageBalanceDisplayed() {
        return ElementAction.isDisplayed(primaryHomeMortgageBalanceInput_xpath, Selectors.xpath);
    }

    public String getTotalLivingCost() {
        return ElementAction.getText(totalLivingCost_xpath, Selectors.xpath);
    }

    public String getTotalIncomeMinusExpenses() {
        return ElementAction.getText(totalIncomeMinusExpenses_xpath, Selectors.xpath);
    }

    public String getTotalIncomeCombined() {
        return ElementAction.getText(totalIncomeCombined_xpath, Selectors.xpath);
    }
}
