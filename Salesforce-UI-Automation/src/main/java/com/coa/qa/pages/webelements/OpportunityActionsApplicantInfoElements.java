package com.coa.qa.pages.webelements;

public interface OpportunityActionsApplicantInfoElements {
    String applicantFirstName_xpath = "//input[@name = 'App_First_Name']";
    String applicantLastName_xpath = "//input[@name = 'App_Last_Name']";
    String applicantEmail_xpath = "//input[@placeholder= 'you@example.com']";
    String applicantSsn_xpath = "//input[@name= 'App_SSN']";
    String applicantBirthdate_xpath = "//input[@name= 'App_Birthdate']";
    String applicantStreetAddress_xpath = "//input[@name= 'App_Street']";
    String applicantCityAddress_xpath = "//input[@name= 'App_City']";
    String applicantStateAddress_xpath = "//span[text() = 'State']/parent::lightning-formatted-rich-text/parent::div/parent::div/lightning-select/div/div/select[@name = 'App_State_0']";
    String applicantZipcode_xpath = "//input[@name = 'App_Zip_Code']";
    String applicantPrimaryPhoneType_xpath = "//select[@name = 'primaryPhoneTypePicklist']";
    String applicantMobile_xpath = "//label[text() = 'Mobile']/parent::lightning-input/div/input[@type= 'tel']";
    String primaryContract_xpath = "//input[@name = 'Primary_Contact__c']";
    String clientEmployerIndustry_xpath = "//button[@name='Client_Employer_Industry__c']";
    String coClientEmployerIndustry_xpath = "//button[@name='CoClient_Employer_Industry__c']";
    String listClientEmployerIndustry_xpath = "//lightning-base-combobox-item/span/span[@class = 'slds-truncate']";
    String nextIncomeBtn_xpath = "//button[@name='save']";
    String coApplicantFirstName_xpath = "//*[@name = 'Coapp_First_Name']";
    String coApplicantLastName_xpath = "//*[@name = 'Coapp_Last_Name']";
    String coApplicantSsn_xpath = "//*[@name = 'Coapp_SSN']";
    String coApplicantBirthdate_xpath = "//*[@name = 'Coapp_Birthdate']";
    String coApplicantStreet_xpath = "//*[@name = 'CoApp_Street']";
    String coApplicantCity_xpath = "//*[@name = 'CoApp_City']";
    String coApplicantZipcode_xpath = "//*[@name = 'CoApp_Postal']";
    String coApplicantState_xpath = "//*[@name = 'Coapp_State_0']";
}
