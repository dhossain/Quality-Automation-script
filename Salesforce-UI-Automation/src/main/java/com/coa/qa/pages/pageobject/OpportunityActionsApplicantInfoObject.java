package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.commons.Utils;
import com.coa.qa.pages.webelements.OpportunityActionsApplicantInfoElements;
import org.openqa.selenium.WebElement;

public class OpportunityActionsApplicantInfoObject implements OpportunityActionsApplicantInfoElements {
    public String getApplicantFirstName() {
        return ElementAction.getAttributeValue(applicantFirstName_xpath, Selectors.xpath);
    }

    public String applicantFirstNameEntered(String firstName) {
        String applicantFirstName = getApplicantFirstName();
        if(applicantFirstName == null) {
            ElementAction.enterText(applicantFirstName_xpath, Selectors.xpath, firstName);
            applicantFirstName = getApplicantFirstName();
        }
        return applicantFirstName;
    }

    public String getApplicantMobileNumber() {
        return ElementAction.getAttributeValue(applicantMobile_xpath, Selectors.xpath);
    }

    public String applicantMobileNumberEntered(String mobileNumber) {
        String applicantMobileNumber = getApplicantMobileNumber();
        if(applicantMobileNumber == null) {
            ElementAction.enterText(applicantMobile_xpath, Selectors.xpath, mobileNumber);
            applicantMobileNumber = getApplicantFirstName();
        }
        return applicantMobileNumber;
    }

    public String getApplicantLastName() {
        return ElementAction.getAttributeValue(applicantLastName_xpath, Selectors.xpath);
    }

    public String applicantLastNameEntered(String lastName) {
        String applicantLastName = getApplicantLastName();
        if(applicantLastName == null) {
            ElementAction.enterText(applicantLastName_xpath, Selectors.xpath, lastName);
            applicantLastName = getApplicantFirstName();
        }
        return applicantLastName;
    }

    public String getApplicantEmail() {
        return ElementAction.getAttributeValue(applicantEmail_xpath, Selectors.xpath);
    }

    public String applicantEmailEntered(String email) {
        String applicantEmail = getApplicantEmail();
        if(applicantEmail == null) {
            ElementAction.enterText(applicantEmail_xpath, Selectors.xpath, email);
            applicantEmail = getApplicantFirstName();
        }
        return applicantEmail;
    }

    public void enterApplicantSSN(String ssn) {
        ElementAction.enterText(applicantSsn_xpath, Selectors.xpath, ssn);
    }

    public String getApplicantBirthDate() { return ElementAction.getAttributeValue(applicantBirthdate_xpath, Selectors.xpath); }

    public void applicantBirthdateEntered(String birthDate) {
        String applicantBirthDate = getApplicantBirthDate();
        if(applicantBirthDate == null) {
            ElementAction.enterText(applicantBirthdate_xpath, Selectors.xpath, birthDate);
        }
    }

    public String getApplicantStreetAddress() { return ElementAction.getAttributeValue(applicantStreetAddress_xpath, Selectors.xpath); }

    public void enterApplicantStreetAddress(String streetAddress) {
        if (getApplicantStreetAddress() == null) {
            ElementAction.enterText(applicantStreetAddress_xpath, Selectors.xpath, streetAddress);
        }
    }

    public String getApplicantCityAddress() { return ElementAction.getAttributeValue(applicantCityAddress_xpath, Selectors.xpath); }

    public void applicantCityAddressEntered(String cityAddress) {
        if (getApplicantCityAddress() == null) {
            ElementAction.enterText(applicantCityAddress_xpath, Selectors.xpath, cityAddress);
        }
    }

    public String getApplicantStateAddress() { return ElementAction.getAttributeValue(applicantStateAddress_xpath, Selectors.xpath); }

    public void selectApplicantStateAddress(String state) {
        if ( getApplicantStateAddress() == null) {
            ElementAction.selectElementByName(applicantStateAddress_xpath, Selectors.xpath, state);
        }
    }

    public String getApplicantPhoneNumberType() { return ElementAction.getAttributeValue(applicantPrimaryPhoneType_xpath, Selectors.xpath); }

    public void selectApplicantPhoneType(String phoneNumber) {
        ElementAction.selectElementByName(applicantPrimaryPhoneType_xpath, Selectors.xpath, phoneNumber);
    }

    public String getApplicantZipcode() { return ElementAction.getAttributeValue(applicantZipcode_xpath, Selectors.xpath); }

    public void enterApplicantZipcodeAddress(String zipCode) {
        if ( getApplicantZipcode() == null ) {
            ElementAction.enterText(applicantZipcode_xpath, Selectors.xpath, zipCode);
        }
    }

    public String getPrimaryContractName() {
        return ElementAction.getText(primaryContract_xpath, Selectors.xpath);
    }

    public boolean isPrimaryContractNameDisplayed() {
        return ElementAction.isDisplayed(primaryContract_xpath, Selectors.xpath);
    }

    public void clickOnClientEmployerIndustry() {
        ElementAction.clickWithJavaScript(clientEmployerIndustry_xpath, Selectors.xpath);
    }

    public void clickOnListOfClientEmployerIndustry(int index) {
        WebElement element = ElementAction.getElementByIndex(listClientEmployerIndustry_xpath, Selectors.xpath, index);
        ElementAction.clickOn(element);
    }

    public void selectClientEmployerIndustry() {
        clickOnClientEmployerIndustry();
        clickOnListOfClientEmployerIndustry(3);
    }

    public void selectCoClientEmployerIndustry() {
        ElementAction.clickWithJavaScript(coClientEmployerIndustry_xpath, Selectors.xpath);
        clickOnListOfClientEmployerIndustry(3);
    }

    public void clickOnNextIncomeBtn() {
        ElementAction.clickWithJavaScript(nextIncomeBtn_xpath, Selectors.xpath);
    }

    public String getCoApplicantFirstName() { return ElementAction.getAttributeValue(coApplicantFirstName_xpath, Selectors.xpath); }

    public String getCoApplicantLastName() { return ElementAction.getAttributeValue(coApplicantLastName_xpath, Selectors.xpath); }

    public String getCoApplicantEmail() { return null; } //TODO

    public String getCoApplicantSsn() { return ElementAction.getAttributeValue(coApplicantSsn_xpath, Selectors.xpath); }

    public String getCoApplicantBirthdate() { return ElementAction.getAttributeValue(coApplicantBirthdate_xpath, Selectors.xpath); }

    public void enterStreetAddress(String street) { ElementAction.enterText(coApplicantStreet_xpath, Selectors.xpath, street); }

    public void enterCity(String city) { ElementAction.enterText(coApplicantCity_xpath, Selectors.xpath, city); }

    public void enterZipcode(String zipcode) { ElementAction.enterText(coApplicantZipcode_xpath, Selectors.xpath, zipcode); }

    public void selectState(int stateIndex) { ElementAction.selectElementByIndex(coApplicantState_xpath, Selectors.xpath, stateIndex); }
}
