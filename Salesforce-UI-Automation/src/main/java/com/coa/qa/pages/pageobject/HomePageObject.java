package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.helper.BaseClass;
import com.coa.qa.pages.webelements.HomePageElements;
import io.cucumber.java.eo.Se;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class HomePageObject extends BaseClass implements HomePageElements {
    private static final Logger logger = LogManager.getLogger(HomePageObject.class);

    public void enterUserName(String userName) {
        ElementAction.enterText(username_xpath, Selectors.xpath, userName);
    }

    public void enterPassword(String password) {
        ElementAction.enterText(password_xpath, Selectors.xpath, password);
    }

    public void clickOnSubmitBtn() {
        ElementAction.clickOnWithExplicitWait(loginBtn_xpath, Selectors.xpath);
    }

    public boolean isHomeBtnDisplay() {
        return ElementAction.isDisplayed(homeBtnLink_xpath, Selectors.xpath);
    }

    public boolean isAdpReminderDisplayed() { return ElementAction.isDisplayed(reminderAdpLoginBtn_xpath, Selectors.xpath); }

    public void clickOnAdpReminderBtn() { ElementAction.clickOn(reminderAdpLoginBtn_xpath, Selectors.xpath);}

    public boolean isLogoDisplayed() {
        return ElementAction.isDisplayed(salesforceLogo_xpath, Selectors.xpath);
    }
}
