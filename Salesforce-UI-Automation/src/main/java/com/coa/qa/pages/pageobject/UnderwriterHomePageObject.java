package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.UnderwriterHomePageElements;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

public class UnderwriterHomePageObject implements UnderwriterHomePageElements {
    private static final Logger logger = LogManager.getLogger(UnderwriterHomePageObject.class);

    public void clickOnFinalApprovalsTab() {
       WebElement element = ElementAction.getElementByIndex(listApprovalsTypeTab_xpath, Selectors.xpath, 1);
       ElementAction.clickOn(element);
    }

    public void clickOnRequestNumber(String opportunityName) {
        int rowNumber = ElementAction.getWebElements(listOfOpportunityInTable_xpath, Selectors.xpath).size();
        logger.info("Total Number of Opportunity in The Approval Queue: " + rowNumber);
        for(int index = 1; index <= rowNumber; index++) {
            String opportunityName_xpath = unassignedFinalApprovalOpportunityList_PreXpath + index + unassignedFinalApprovalOpportunityList_PostXpath;
            String requestNumber_xpath = unassignedFinalApprovalRequestNumberList_PreXpath + index + unassignedFinalApprovalRequestNumberList_PostXpath;
            String opportunity = ElementAction.getText(opportunityName_xpath, Selectors.xpath);
            if (opportunity.equalsIgnoreCase(opportunityName)) {
                ElementAction.clickOn(requestNumber_xpath, Selectors.xpath);
            }
        }
    }
}
