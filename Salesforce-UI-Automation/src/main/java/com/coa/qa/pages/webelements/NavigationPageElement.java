package com.coa.qa.pages.webelements;

public interface NavigationPageElement {
    String navigationMenu = "//button[@title='Show Navigation Menu']//lightning-primitive-icon";
    String navigationMenuOptionsList = "//div[@class='slds-media slds-listbox__option slds-listbox__option_entity slds-p-around_none']";
    String leadsBtnLink = "//a[@title = 'Leads']";
    String viewProfile_xpath = "//span[@class = 'userProfileCardTriggerRoot oneUserProfileCardTrigger']"; // "//span[@class = 'userProfileCardTriggerRoot oneUserProfileCardTrigger' and not(@disabled)]/button";
    String accountOwnerName_xpath = "//h1/a[@class = 'profile-link-label']";
    String logOutLink_xpath = "//a[@class = 'profile-link-label logout uiOutputURL' and not(@disabled)]";
}
