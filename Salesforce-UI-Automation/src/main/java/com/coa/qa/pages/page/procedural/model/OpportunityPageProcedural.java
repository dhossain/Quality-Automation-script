package com.coa.qa.pages.page.procedural.model;

import com.coa.qa.pages.pageobject.OpportunityPageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class OpportunityPageProcedural {
    private static final Logger logger = LogManager.getLogger(OpportunityPageProcedural.class);
    private OpportunityPageObject opportunityPageObject = new OpportunityPageObject();

    public String getOpportunityType() { return opportunityPageObject.getOpportunityType(); }
}
