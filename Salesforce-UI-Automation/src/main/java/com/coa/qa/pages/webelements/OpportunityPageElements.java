package com.coa.qa.pages.webelements;

public interface OpportunityPageElements {
    String householdAccountName_xpath = "//span[text()= 'Household']/parent::div/parent::div/div[2]/span/slot/force-lookup/div/records-hoverable-link/div/a/slot/slot/span";
    String coApplicantAccountName_xpath = "//span[text()= 'Co-Applicant']/parent::div/parent::div/div[2]/span/slot/force-lookup/div/records-hoverable-link/div/a/slot/slot/span";
    String applicantAccountName_xpath = "//span[text()= 'Applicant']/parent::div/parent::div/div[2]/span/slot/force-lookup/div/records-hoverable-link/div/a/slot/slot/span";
    String opportunityNameInTitleModel_xpath = "//div[text() = 'Opportunity']/parent::h1/slot/slot/lightning-formatted-text";
    String opportunityConvertSuccessMsg_xpath = "//div[@class = 'title']/span";
    String editMarketingVendor_xpath = "//button[@title = 'Edit Marketing Vendor']";
    String saveEditBtn_xpath = "//lightning-button/button[@name = 'SaveEdit']";
    String stageValue_xpath = "//span[text()='Stage']/parent::div/parent::div/div[2]/span/slot/lightning-formatted-text";
    String subStageValue_xpath = "//span[text()='Sub-Stage']/parent::div/parent::div/div[2]/span/slot/lightning-formatted-text";
    String editStageBtn_xpath = "//button[@title = 'Edit Stage']";
    String editSubStageBtn_xpath = "//button[@title = 'Edit Sub-Stage']";
    String subStageInput_xpath = "//label[text()='Sub-Stage']/parent::lightning-combobox/div/lightning-base-combobox/div/div/button";
    String stageInput_xpath = "//label[text()='Stage']/parent::lightning-combobox/div/lightning-base-combobox/div/div/button";
    String listOfStage_xpath = "//label[text()= 'Stage']/parent::lightning-combobox/div/lightning-base-combobox/div/div[2]/lightning-base-combobox-item/span/span";
    String listOfSubStage_xpath = "//label[text()= 'Sub-Stage']/parent::lightning-combobox/div/lightning-base-combobox/div/div[2]/lightning-base-combobox-item/span/span";
    String opportunityRecordType_xpath = "//span[text() = 'Opportunity Record Type']/parent::div/parent::div/div[2]/span/slot/records-record-type/div/div/span";
}
