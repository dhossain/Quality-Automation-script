package com.coa.qa.pages.webelements;

public interface ProgramSummaryElements {
    String newMonthlyPayment_xpath = "//input[@name = 'New_Monthly_Payment__c']";
    String newTermLength_xpath = "//input[@name = 'Max_Length__c']";
    String closeProgramSummaryBtn_xpath = "//button[text()= 'Close']";
}
