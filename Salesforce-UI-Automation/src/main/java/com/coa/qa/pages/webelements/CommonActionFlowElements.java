package com.coa.qa.pages.webelements;

public interface CommonActionFlowElements {
    String debtsCreditReportsBtn_xpath = "//a[text() = 'Debts & Credit Reports']";
    String debtsTable_xpath = "//table[@class = 'slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr";
    String actionFlowTabsBtn_xpath = "//div[@class = 'slds-button-group cEnrollmentActions']/button[@class = 'slds-button slds-button_neutral']";
    String nextIncomeBtn_xpath = "//button[@name='save']";
    String incomeExpensesCompletedMessages_xpath = "//lightning-formatted-rich-text/span/p/span";
    String actionFlowNextBtn_xpath = "(//button[text()='Next'])[1]";
    String listOfNextBtn_xpath = "//button[text()='Next']";
    String saveBtn_xpath = "(//button[text()='Save'])[1]";
    String listOfCheckmarkIcon_xpath = "//div/lightning-icon[@class = 'slds-icon-utility-success slds-icon_container']";
    String populatedStartDate_xpath = "//span[text() = 'Program Start Date']/parent::div/parent::div/div[2]/span/slot/lightning-formatted-text";
    String populatedDraftFrequency_xpath = "//span[text() = 'Draft Frequency']/parent::div/parent::div/div[2]/span/slot/lightning-formatted-text";
    String tabList_xpath = "//lightning-tab-bar/ul/li/a";
    String processor_xpath = "//span[text() = 'Processor']/parent::div/parent::div/div[2]/span/slot/force-lookup/div/records-hoverable-link/div/a/slot/slot/span";
    String callScriptTask_xpath = "//*[contains(text(), 'CALL SCRIPT')]";
}
