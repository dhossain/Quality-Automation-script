package com.coa.qa.pages.webelements;

public interface OpportunityAddDebtElements {
    String relatedCreditorSearch_xpath = "//label[text() = 'Related Creditor Currrent']/parent::lightning-grouped-combobox/div/div/lightning-base-combobox/div/div/input[@placeholder = 'Search Creditor Profiles...']";
    String listOfRelatedCreditor_xpath = "//lightning-base-combobox-formatted-text";
    String accountNumber_xpath = "//input[@name = 'Account_Number']";
    String accountDesignator_xpath = "//button[@name = 'Account Designator']";
    String accountDesignatorIndividual_xpath = "//span[@title = 'Individual']";
    String accountDesignatorJoint_xpath = "//span[@title = 'Joint']";
    String accountDesignatorAuthorizedUser_xpath = "//span[@title = 'Authorized User']";
    String debtOwner_xpath = "//button[@name = 'Debt Owner']";
    String applicantDebtOwner_xpath = "//span[@title = 'Applicant']";
    String daysDelinquent_xpath = "//button[@name = 'Days Delinquent']";
    String daysDelinquent120Days_xpath = "//span[@title = '120']";
    String debtTypeInput_xpath = "//button[@name = 'Debt Type']";
    String debtType_xpath = "//button[@name = 'Debt Type']/parent::div/parent::div/div[2]/lightning-base-combobox-item/span/span";
    String legalOrCollectionInput_xpath = "//button[@name = 'Legal or Collection']";
    String legalOrCollectionValue_xpath = "//button[@name = 'Legal or Collection']/parent::div/parent::div/div[2]/lightning-base-combobox-item/span/span";
    String debtBalance_xpath = "//input[@name = 'Balance']";
    String relatedCreditorNameInDebtTable_xpath = "//table[@class = 'slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr[1]/td[4]/lightning-primitive-cell-factory/span/div/lightning-primitive-custom-cell/force-lookup/div/a";
}
