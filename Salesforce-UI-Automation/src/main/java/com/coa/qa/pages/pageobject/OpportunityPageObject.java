package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.commons.MarketingVendor;
import com.coa.qa.pages.webelements.OpportunityPageElements;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OpportunityPageObject implements OpportunityPageElements {
    private static final Logger logger = LogManager.getLogger(OpportunityPageObject.class);
    private LeadsPageObject leadsPageObject = new LeadsPageObject();

    public String getHouseholdAccountName() {
        return ElementAction.getText(householdAccountName_xpath, Selectors.xpath);
    }

    public String getCoApplicantAccountName() {
        return ElementAction.getText(coApplicantAccountName_xpath, Selectors.xpath);
    }

    public String getApplicantAccountName() {
        return ElementAction.getText(applicantAccountName_xpath, Selectors.xpath);
    }

    public void clickOnEditMarketingVendor() {
        ElementAction.clickOn(editMarketingVendor_xpath, Selectors.xpath);
    }

    public void clickOnEditSaveBtn() {
        ElementAction.clickOn(saveEditBtn_xpath, Selectors.xpath);
    }

    public String getStageValue() {
        logger.info("Getting Stage Value");
        return ElementAction.getText(stageValue_xpath, Selectors.xpath);
    }

    public String getSubStageValue() {
        logger.info("Getting Sub-Stage Value");
        return ElementAction.getText(subStageValue_xpath, Selectors.xpath);
    }

    private void clickOnStageEditBtn() {
        ElementAction.clickWithJavaScript(editStageBtn_xpath, Selectors.xpath);
    }

    private void clickOnStage() {
        ElementAction.clickWithJavaScript(stageInput_xpath, Selectors.xpath);
    }

    private void clickOnSubStage() {
        ElementAction.clickWithJavaScript(subStageInput_xpath, Selectors.xpath);
    }

    public void clickOnTheListOfStage(String stageName) {
        List<WebElement> elements = ElementAction.getWebElements(listOfStage_xpath, Selectors.xpath);
        WebElement element = ElementAction.getElementFromListByText(elements, stageName);
        ElementAction.clickWithJavaScript(element);
    }

    public void clickOnTheListOfSubStage(String subStageName) {
        List<WebElement> elements = ElementAction.getWebElements(listOfSubStage_xpath, Selectors.xpath);
        WebElement element = ElementAction.getElementFromListByText(elements, subStageName);
        ElementAction.clickWithJavaScript(element);
    }

    public boolean setStatusStageSubStage(String stage, String subStage) {
        clickOnStageEditBtn();
        clickOnStage();
        clickOnTheListOfStage(stage);
        clickOnSubStage();
        clickOnTheListOfSubStage(subStage);
        leadsPageObject.clickOnSaveBtn();
        return getStageValue().equalsIgnoreCase(stage)
                && getSubStageValue().equalsIgnoreCase(subStage);
    }

    public String getOpportunityType() { return ElementAction.getText(opportunityRecordType_xpath, Selectors.xpath); }
}
