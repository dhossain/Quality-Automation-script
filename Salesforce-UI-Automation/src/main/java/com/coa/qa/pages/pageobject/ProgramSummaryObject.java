package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.ProgramSummaryElements;

public class ProgramSummaryObject implements ProgramSummaryElements {

    public boolean isNewMonthlyPaymentDisplayed() {
        return ElementAction.isDisplayed(newMonthlyPayment_xpath, Selectors.xpath);
    }

    public void enterNewMonthlyPayment(String amount) {
        ElementAction.enterText(newMonthlyPayment_xpath, Selectors.xpath, amount);
    }

    public String getNewMonthlyPayment() {
        return ElementAction.getAttributeValue(newMonthlyPayment_xpath, Selectors.xpath);
    }

    public void enterNewTermLength(String term) {
        ElementAction.enterText(newTermLength_xpath, Selectors.xpath, term);
    }

    public String getNewTermLength() {
        return ElementAction.getAttributeValue(newTermLength_xpath, Selectors.xpath);
    }

    public void clickOnCloseBtn() {
        ElementAction.clickWithJavaScript(closeProgramSummaryBtn_xpath, Selectors.xpath);
    }
}
