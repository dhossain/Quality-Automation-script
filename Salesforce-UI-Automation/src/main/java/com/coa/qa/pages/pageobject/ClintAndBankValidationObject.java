package com.coa.qa.pages.pageobject;

import com.clearone.commonUtils.Utils;
import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.commons.WebUtils;
import com.coa.qa.pages.webelements.ClintAndBankValidationElements;

public class ClintAndBankValidationObject implements ClintAndBankValidationElements {
    private CommonActionFlowObject commonActionFlowObject = new CommonActionFlowObject();

    public boolean isBankNameDisplayed() {
        return ElementAction.isDisplayed(bankName_xpath, Selectors.xpath);
    }

    private Boolean isSendContractBtnDisplayed() {
        return ElementAction.isDisplayed(sendContractBtn_xpath, Selectors.xpath);
    }
    public void enterBankName(String bankName) {
        ElementAction.enterText(bankName_xpath, Selectors.xpath, bankName);
    }

    private void enterRoutingNumber(String routingNumber) {
        ElementAction.enterText(routingNumber_xpath, Selectors.xpath, routingNumber);
    }

    private void enterConfirmRoutingNumber(String confirmRoutingNumber) {
        ElementAction.enterText(confirmRoutingNumber_xpath, Selectors.xpath, confirmRoutingNumber);
    }

    private void enterAccountNumber(String accountNumber) {
        ElementAction.enterText(accountNumber_xpath, Selectors.xpath, accountNumber);
    }

    private void enterConfirmAccountNumber(String confirmAccountNumber) {
        ElementAction.enterText(confirmAccountNumber_xpath, Selectors.xpath, confirmAccountNumber);
    }

    private void selectBankType() {
        ElementAction.selectElementByIndex(bankType_xpath, Selectors.xpath, 1);
    }

    private void enterMotherName(String motherName) {
        ElementAction.enterText(applicantMotherMiddleName_xpath, Selectors.xpath, motherName);
    }

    private void completeRoutingNumber(String routingNumber) {
        enterRoutingNumber(routingNumber);
        enterConfirmRoutingNumber(routingNumber);
    }

    private void completeAccountNumber(String accountNumber) {
        enterAccountNumber(accountNumber);
        enterConfirmAccountNumber(accountNumber);
    }

    private String getSuccessMsg() {
        return ElementAction.getText(accountVerifySuccessMsg_xpath, Selectors.xpath);
    }

    private String getClintName() {
        String clintFirstName = ElementAction.getText(clintValidationFirstName_xpath, Selectors.xpath);
        String clintLastName = ElementAction.getText(clintValidationLastName_xpath, Selectors.xpath);
        return clintFirstName +" " + clintLastName;
    }

    private String getLast4DigitSsn() {
        return ElementAction.getText(clintValidationSsn_xpath, Selectors.xpath);
    }

    private String getBirthday() {
        return ElementAction.getText(clintValidationBirthday_xpath, Selectors.xpath);
    }

    private String getAddressLine1() {
        return ElementAction.getText(clintValidationAddressLine1, Selectors.xpath);
    }

    private String getAddressLine2() {
        return ElementAction.getText(clintValidationAddressLine2, Selectors.xpath);
    }

    private boolean isClintValidationFailIconDisplayed() {
        return ElementAction.isDisplayed(clintValidationFailIcon_xpath, Selectors.xpath);
    }

    private String getMsgToClickForSendContractBtn() {
        return ElementAction.getText(nextToProcessMsgToSendContract_xpath, Selectors.xpath);
    }

    public String bankAccountValidation(String bankName, String routingNumber, String accountNumber) {
        if (isBankNameDisplayed()) {
            enterBankName(bankName);
            completeRoutingNumber(routingNumber);
            completeAccountNumber(accountNumber);
            enterMotherName(Utils.getLastName());
            selectBankType();
            commonActionFlowObject.clickOnNextBtnInAction();
        }
        return getSuccessMsg();
    }

    public boolean clintValidate() {
        commonActionFlowObject.clickOnNextBtnInAction();
        return getClintName() != null
                && getLast4DigitSsn() != null
                && getBirthday() != null
                && getAddressLine1() != null
                && getAddressLine2() != null;
    }

    public boolean clintBankValidation(String bankName, String routingNumber, String accountNumber) {
        boolean isSendContractDisplayed = false;
        WebUtils.pageReload();
        if (bankAccountValidation(bankName, routingNumber, accountNumber) != null) {
            if (clintValidate()) {
                commonActionFlowObject.clickOnNextBtnInAction();
                if (getSuccessMsg() != null && isClintValidationFailIconDisplayed()) {
                    commonActionFlowObject.clickOnNextBtnInAction();
                    if (getMsgToClickForSendContractBtn() != null) {
                        commonActionFlowObject.clickOnNextBtnInAction();
                        isSendContractDisplayed = isSendContractBtnDisplayed();
                    }
                }
            }
        }
        return isSendContractDisplayed;
    }
}
