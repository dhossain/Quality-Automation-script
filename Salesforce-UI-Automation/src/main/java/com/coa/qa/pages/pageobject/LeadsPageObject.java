package com.coa.qa.pages.pageobject;

import com.clearone.commonUtils.Utils;
import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.LeadsPageElements;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LeadsPageObject implements LeadsPageElements {
    private static final Logger logger = LogManager.getLogger(LeadsPageObject.class);
    private String FIRST_NAME = Utils.getFirstName();
    private String LAST_NAME = Utils.getLastName();

    public void clickOnTheNewBtn() {
        ElementAction.clickOnWithExplicitWait(newBtn_xpath, Selectors.xpath);
    }
    public void enterMinimumCustomerInfo(String firstName, String lastName, String email) {
        clickOnTheNewBtn();
        ElementAction.enterText(firstName_xpath, Selectors.xpath, firstName);
        ElementAction.enterText(middleName_xpath, Selectors.xpath, "Automation");
        ElementAction.enterText(lastName_xpath, Selectors.xpath, lastName);
        ElementAction.enterText(email_xpath, Selectors.xpath, email);
    }

    public void createLeadWithCoApplicant(
            String applicantFirstName,
            String applicantLastName,
            String applicantEmail,
            String coApplicantFirstName,
            String coApplicantLastName,
            String coApplicantEmail,
            String marketingVendor) {
        try {
            enterMinimumCustomerInfo(applicantFirstName, applicantLastName, applicantEmail);
            //enterMarketingVendor(marketingVendor);
            enterCoApplicantFirstName(coApplicantFirstName);
            enterCoApplicantLastName(coApplicantLastName);
            enterCoApplicantEmail(coApplicantEmail);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Not able to create a leads !!!");
        }

    }
    public void enterRequireCustomerInfo(String lastName, String email) {
        ElementAction.enterText(lastName_xpath, Selectors.xpath, lastName);
        ElementAction.enterText(email_xpath, Selectors.xpath, email);
    }

    public void clickOnSaveBtn() {
        ElementAction.clickOn(saveBtn_xpath, Selectors.xpath);
    }

    public boolean isCustomerNameDisplayed() {
        return ElementAction.isDisplayed(customerNameInLeads_xpath, Selectors.xpath);
    }

    public void closeLeadTab() {
        ElementAction.clickOn(closeTabBtn_xpath, Selectors.xpath);
    }

    public boolean isErrSnagDisplayed() {
        return ElementAction.isDisplayed(errSnagMsgPopUp_xpath, Selectors.xpath);
    }

    public String getErrSnagMsg() {
        return ElementAction.getText(errSnagMsgPopUp_xpath, Selectors.xpath);
    }

    @Deprecated
    public void clickOnConvertDropDownBtn() {
        ElementAction.clickOn(convertDropDownBtn_xpath, Selectors.xpath);
    }

    @Deprecated
    public void clickOnConvertBtn() {
        ElementAction.clickOn(convertBtn_xpath, Selectors.xpath);
    }

    @Deprecated
    public boolean isConvertTabDisplayed() {
        return ElementAction.isDisplayed(convertTab_xpath, Selectors.xpath);
    }

    @Deprecated
    public String getTextLastNameOfConvertTab() {
        return ElementAction.getAttributeValue(lastNameOnConvertTab_xpath, Selectors.xpath);
    }

    @Deprecated
    public void clearLastNameOnConvertTab() {
        ElementAction.clearText(lastNameOnConvertTab_xpath, Selectors.xpath);
    }

    @Deprecated
    public void clickOnConvertOpportunityBtn() {
        ElementAction.clickOn(convertOpportunityBtn_xpath, Selectors.xpath);
    }

    @Deprecated
    public String getSuccessMsgOpportunityConvert() {
        return ElementAction.getText(opportunityConvertSuccessMsg_xpath, Selectors.xpath);
    }

    @Deprecated()
    public void closeOpportunityWindow() { ElementAction.clickOn(xBtnOpportunityWindow, Selectors.xpath);}

    public void clickOnTheManagerConversionBtn() {
        ElementAction.clickWithJavaScriptOnStaleElement(managerConversionBtn_xpath, Selectors.xpath);
    }

    public void clickOnNextBtnInManagerConversion() {
        ElementAction.clickOn(managerConversionNxtBtn_xpath, Selectors.xpath);
    }

    public void enterPhoneNumberInManagerConversion(String phoneNumber) {
        ElementAction.enterText(phoneNumberManagerConversion_xpath, Selectors.xpath, phoneNumber);
    }

    public boolean selectHardshipManagerConversion() {
        try {
            WebElement hardShipName = ElementAction.getElementByIndex(hardshipList_xpath, Selectors.xpath, 0);
            ElementAction.clickOn(hardShipName);
            ElementAction.clickOn(moveSectionToChoseIcon, Selectors.xpath);
            List chosenHardshipLst = ElementAction.getWebElements(chosenHardshipList_xpath, Selectors.xpath);
            return chosenHardshipLst.size() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Hardship is NOT completed !!!");
            return false;
        }
    }

    public void enterWhoDidInManagerConversion(String whoDidTxt) {
        ElementAction.enterText(whoDidHappenInput_xpath, Selectors.xpath, whoDidTxt);
    }

    public void enterWhenDidInManagerConversion(String whenDidTxt) {
        ElementAction.enterText(whenDidHappenTextArea_xpath, Selectors.xpath, whenDidTxt);
    }

    public void enterHowMuchDidInManagerConversion(String howMuchDidTxt) {
        ElementAction.enterText(howMuchAffectTextArea_xpath, Selectors.xpath, howMuchDidTxt);
    }

    public void clickOnConvertBtnLeadInManagerConversion() {
        ElementAction.clickOn(convertLeadBtn_xpath, Selectors.xpath);
    }

    public void enterCoApplicantFirstName(String firstName) {
        ElementAction.enterText(coApplicantFirstName_xpath, Selectors.xpath, firstName);
    }

    public void enterCoApplicantLastName(String lastName) {
        ElementAction.enterText(coApplicantLastName_xpath, Selectors.xpath, lastName);
    }

    public void enterCoApplicantEmail(String email) {
        ElementAction.enterText(coApplicantEmail_xpath, Selectors.xpath, email);
    }

    public void enterMarketingVendor(String marketingVendor) {
        ElementAction.enterText(marketingVendorText_xpath, Selectors.xpath, marketingVendor);
        List <WebElement> elements = ElementAction.getWebElements(listOfMarketingVendor_xpath, Selectors.xpath);
        if (ElementAction.getElementFromListByText(elements, marketingVendor) != null) {
            ElementAction.clickOn(ElementAction.getElementFromListByText(elements, marketingVendor));
        }
    }

    public String getOpportunityTitleName() {
        return ElementAction.getText(opportunityNameInTitleModel_xpath, Selectors.xpath);
    }

    public boolean isOpportunityTitleDisplayed() {
        return ElementAction.isDisplayed(opportunityNameInTitleModel_xpath, Selectors.xpath);
    }

    public void clickOnChangeOwnerIcon() {
        ElementAction.clickOn(ownerChangeIcon_xpath, Selectors.xpath);
    }

    public void enterOwnerName(String firstName, String lastName) {
        String fullName = firstName + " " + lastName;
        ElementAction.clickOn(ownerChangeSearchBox_xpath, Selectors.xpath);
        ElementAction.enterText(ownerChangeSearchBox_xpath, Selectors.xpath, fullName);
    }

    public void selectOwner(String firstName, String lastName) {
        List <WebElement> elements = ElementAction.getWebElements(listOfOwner_xpath, Selectors.xpath);
        if (ElementAction.getElementFromListByText(elements, firstName) != null
                && ElementAction.getElementFromListByText(elements, lastName) != null){
            ElementAction.clickOn(ElementAction.getElementFromListByText(elements, firstName));
        }
    }

    public void clickOnChangeOwnerBtn() {
        ElementAction.clickOn(changeOwnerBtn_xpath, Selectors.xpath);
    }

    public String getOwnerName() {
        return ElementAction.getText(changedOwnerName_xpath, Selectors.xpath);
    }

    public boolean isClearOneSalesDisplayed() { return ElementAction.isDisplayed(clearOneSalesTitle_xpath, Selectors.xpath); }

    public void enterCoApplicantMobileNo(String mobileNumber) { ElementAction.enterText(coApplicantMobile_xpath, Selectors.xpath, mobileNumber); }

    public void enterCoApplicantBirthdate(String birthDate) { ElementAction.enterText(coApplicantBirthdate_xpath, Selectors.xpath, birthDate); }

    public void clickOnAddressSameAsApplicantIcon() { ElementAction.clickOn(addressAsApplicant_xpath, Selectors.xpath); }

    public void clickOnEditCoApplicantBtn() { ElementAction.clickWithJavaScript(coApplicantEditBtn_xpath, Selectors.xpath); }

    public void enterCoApplicantSsn(String ssn) { ElementAction.enterText(coApplicantSsn_xpath, Selectors.xpath, ssn); }

    public String getCoApplicantLastName() { return ElementAction.getText(coApplicantLastNameValue_xpath, Selectors.xpath); }

    public List<String> getListOfDuplicateLeads() {
        return ElementAction.getWebElements(listOfDuplicateLeads_xpath, Selectors.xpath).stream()
                .map(leadName -> leadName.getText())
                .collect(Collectors.toList());
    }

    public String getInContractFileName() { return ElementAction.getText(inContractFileName_xpath, Selectors.xpath); }

    public List<String> getListOfCreatedTask() {
        return ElementAction.getWebElements(listOfTask_xpath, Selectors.xpath).stream()
                .map(taskName -> taskName.getText())
                .collect(Collectors.toList());
    }
}
