package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.OpportunityAddDebtElements;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OpportunityAddDebtObject implements OpportunityAddDebtElements {
    private void enterRelatedCreditor(String relatedCreditor) {
        ElementAction.enterText(relatedCreditorSearch_xpath, Selectors.xpath, relatedCreditor);
    }

    private void clickOnRelatedCreditor(String relatedCreditor) {
        List<WebElement> elements = ElementAction.getWebElements(listOfRelatedCreditor_xpath, Selectors.xpath);
        WebElement element = ElementAction.getElementFromListByText(elements, relatedCreditor);
        ElementAction.clickOn(element);
    }

    public void enterVillasAtChaseRelatedCreditor(String relatedCreditor) {
        enterRelatedCreditor(relatedCreditor);
        clickOnRelatedCreditor(relatedCreditor);
    }

    public void enterAccountNumber(String accountNumber) {
        ElementAction.enterText(accountNumber_xpath, Selectors.xpath, accountNumber);
    }

    private void clickOnAccountDesignator() {
        ElementAction.clickOn(accountDesignator_xpath, Selectors.xpath);
    }
    public void selectIndividualAccountDesignator() {
        clickOnAccountDesignator();
        ElementAction.clickOn(accountDesignatorIndividual_xpath, Selectors.xpath);
    }

    private void clickOnDebtOwnerInput() {
        ElementAction.clickOn(debtOwner_xpath, Selectors.xpath);
    }

    public void selectApplicantDebtOwner() {
        clickOnDebtOwnerInput();
        ElementAction.clickOn(applicantDebtOwner_xpath, Selectors.xpath);
    }

    private void clickOnDaysDelinquent() {
        ElementAction.clickOn(daysDelinquent_xpath, Selectors.xpath);
    }

    public void select120DaysDelinquent() {
        clickOnDaysDelinquent();
        ElementAction.clickOn(daysDelinquent120Days_xpath, Selectors.xpath);
    }

    private void clickOnDebtTypeInput() {
        ElementAction.clickOn(debtTypeInput_xpath, Selectors.xpath);
    }

    public void selectDebtType() {
        clickOnDebtTypeInput();
        WebElement element = ElementAction.getElementByIndex(debtType_xpath, Selectors.xpath, 3);
        ElementAction.clickOn(element);
    }

    private void clickOnLegalOrCollectionInput() {
        ElementAction.clickOn(legalOrCollectionInput_xpath, Selectors.xpath);
    }

    public void selectLegalOrCollection() {
        clickOnLegalOrCollectionInput();
        WebElement element = ElementAction.getElementByIndex(legalOrCollectionValue_xpath, Selectors.xpath, 1);
        ElementAction.clickOn(element);
    }

    public void enterDebtBalance(String balanceAmount) {
        ElementAction.enterText(debtBalance_xpath, Selectors.xpath, balanceAmount);
    }

    public String getCreditorNameFromDebtTable() {
        return ElementAction.getText(relatedCreditorNameInDebtTable_xpath, Selectors.xpath);
    }
}
