package com.coa.qa.pages.webelements;

public interface UnderwriterHomePageElements {
    String listApprovalsTypeTab_xpath = "//li/a/span[@class = 'title']";

    //Unassigned Final-Approval Table
    String listOfOpportunityInTable_xpath = "//h2/a[text() = 'Unassigned at Final Approval']/parent::h2/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/td[7]/span/a";
    String unassignedFinalApprovalOpportunityList_PreXpath = "//h2/a[text() = 'Unassigned at Final Approval']/parent::h2/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr[";
    String unassignedFinalApprovalOpportunityList_PostXpath = "]/td[7]/span/a";
    String unassignedFinalApprovalRequestNumberList_PreXpath = "//h2/a[text() = 'Unassigned at Final Approval']/parent::h2/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr[";
    String unassignedFinalApprovalRequestNumberList_PostXpath = "]/th/span/a";
}
