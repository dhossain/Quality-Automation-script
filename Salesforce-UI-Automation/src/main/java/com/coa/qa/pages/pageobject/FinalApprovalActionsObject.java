package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.FinalApprovalActionsElements;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FinalApprovalActionsObject implements FinalApprovalActionsElements {
    private static final Logger logger = LogManager.getLogger(FinalApprovalActionsObject.class);

    private void enterApprovalCommand(String note) {
        ElementAction.enterText(approvalCommentsInput_xpath, Selectors.xpath, note);
    }

    private void clickOnSubmitBtn() {
        ElementAction.clickWithJavaScript(submitBtn_xpath, Selectors.xpath);
    }

    private String getResultMsg() {
        return ElementAction.getText(lockedMsg_xpath, Selectors.xpath);
    }

    private String getApprovalMsg() {
        return ElementAction.getText(approvalMsg_xpath, Selectors.xpath);
    }

    public boolean finalApprovalSubmit(String resultMsg) {
        enterApprovalCommand("Test Note For Final Approval");
        clickOnSubmitBtn();
        logger.info("Final Approval Message: " + getApprovalMsg() + " " + getResultMsg());
        return getResultMsg().equalsIgnoreCase(resultMsg);
    }
}
