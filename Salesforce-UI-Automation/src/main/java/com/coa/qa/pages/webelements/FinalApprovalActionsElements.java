package com.coa.qa.pages.webelements;

public interface FinalApprovalActionsElements {
    String approvalCommentsInput_xpath = "//textarea[@class = 'slds-textarea']";
    String lockedMsg_xpath = "//div/p/span/parent::p/b";
    String approvalMsg_xpath = "//div[@class = 'slds-rich-text-editor__output uiOutputRichText forceOutputRichText']/p/span";
    String submitBtn_xpath = "//button[text() = 'Submit']";
}
