package com.coa.qa.pages.webelements;

public interface OpportunityActionIncomeExpensesElements {
    String applicantNetSalaryText_xpath = "//lightning-input/label[text() = 'Applicant Net Salary']";
    String applicantNetSalary_xpath = "//input[@name='Applicant_Net_Salary__c']";
    String applicantRent_xpath = "//input[@name='Rent__c']";
    String healthInsuranceInput_xpath = "//input[@name='Health_Insurance__c']";
    String cableInternetInput_xpath = "//input[@name='Cable_Internet__c']";
    String marketValuePrimaryHouseInput_xpath = "//input[@name='Market_Value_of_Primary_House__c']";
    String primaryHomeMortgageBalanceInput_xpath = "//input[@name='Primary_Home_Mortgage_Balance__c']";
    String totalLivingCost_xpath = "//input[@name = 'Total_Living_Cost__c']";
    String totalIncomeMinusExpenses_xpath = "//input[@name = 'Total_Income_Minus_Expenses__c']";
    String totalIncomeCombined_xpath = "//input[@name = 'Total_Income_Combined__c']";
}
