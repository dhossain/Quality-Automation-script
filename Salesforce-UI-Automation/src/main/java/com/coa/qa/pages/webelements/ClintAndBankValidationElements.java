package com.coa.qa.pages.webelements;

public interface ClintAndBankValidationElements {
    String sendContractBtn_xpath = "//lightning-button/button[text() = 'Send Contract']";
    String bankName_xpath = "//input[@name = 'Bank_Name']";
    String routingNumber_xpath = "//input[@name = 'Routing_Number_Inherited']";
    String confirmRoutingNumber_xpath = "//input[@name = 'Routing_Number']";
    String applicantMotherMiddleName_xpath = "//input[@name = 'Applicant_Mother_s_Maiden_Name']";
    String bankType_xpath = "//select[@name = 'Bank_Type']";
    String accountNumber_xpath = "//input[@name = 'Account_Number_Inherited']";
    String confirmAccountNumber_xpath = "//input[@name = 'Account_Number']";
    String accountVerifySuccessMsg_xpath = "//lightning-formatted-rich-text/span/p/b";
    String clintValidationFirstName_xpath = "(//lightning-formatted-rich-text/span/p/b/parent::p/span)[1]";
    String clintValidationLastName_xpath = "(//lightning-formatted-rich-text/span/p/b/parent::p/span)[2]";
    String clintValidationSsn_xpath = "(//lightning-formatted-rich-text/span/p/b/parent::p/span)[4]";
    String clintValidationBirthday_xpath = "(//lightning-formatted-rich-text/span/p/b/parent::p/span)[5]";
    String clintValidationAddressLine1 = "//lightning-formatted-rich-text/span/p/b[text() = 'Mailing Address:']/parent::p/parent::span/p[3]/span";
    String clintValidationAddressLine2 = "//lightning-formatted-rich-text/span/p/b[text() = 'Mailing Address:']/parent::p/parent::span/p[4]/span";
    String clintValidationFailIcon_xpath = "//span[@class = 'main']/img";
    String nextToProcessMsgToSendContract_xpath = "//lightning-formatted-rich-text/span/p[3]/b";
}
