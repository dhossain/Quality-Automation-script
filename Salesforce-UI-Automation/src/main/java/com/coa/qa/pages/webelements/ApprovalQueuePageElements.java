package com.coa.qa.pages.webelements;

public interface ApprovalQueuePageElements {
    String takeOwnership_xpath = "//span[text() = 'Take Ownership']";
    String nextBtn_xpath = "//button[text()='Next']";
    String outcomeRequestList_xpath = "//legend[text() = 'Please select the outcome of the request']/parent::fieldset/div/div/label/span/div/span[@class = 'slds-text-heading_medium slds-m-bottom_x-small']";
    String textCommandArea_xpath = "//lightning-textarea/div/textarea";
    String approvalRequestWas_xpath = "//lightning-formatted-rich-text[@class = 'slds-rich-text-editor__output']/span/p/span";
    String statusAndDate_xpath = "//span[text() = 'This approval request was ']/parent::p/b";

    String detailsTab_xpath = "//lightning-tab-bar/ul/li[@title = 'Details']/a";
    String opportunityName_xpath = "//span[text() = 'Opportunity']/parent::div/parent::div/div[2]/span/slot/force-lookup/div/records-hoverable-link/div/a/slot/slot/span";
    String oppStage_xpath = "//span[text() = 'Opp Stage']/parent::div/parent::div/div[2]/span/slot/records-formula-output/slot/lightning-formatted-text";
    String currentSubStage_xpath = "//span[text() = 'Current Substage']/parent::div/parent::div/div[2]/span/slot/records-formula-output/slot/lightning-formatted-text";
}
