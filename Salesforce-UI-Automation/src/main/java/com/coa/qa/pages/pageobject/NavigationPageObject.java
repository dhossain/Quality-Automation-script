package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.GetWebElement;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.LeadsPageElements;
import com.coa.qa.pages.webelements.NavigationPageElement;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.List;

public class NavigationPageObject implements NavigationPageElement, LeadsPageElements {
    private static final Logger logger = LogManager.getLogger(NavigationPageObject.class);
    private GetWebElement getWebElement = new GetWebElement();

    public void clickOnNavigationMenu() {
        ElementAction.clickOnWithExplicitWait(navigationMenu, Selectors.xpath);
    }
    public void clickOnTheLeadTab() throws Exception {
        List<WebElement> elements =  ElementAction.getWebElements(navigationMenuOptionsList, Selectors.xpath);
        WebElement element = ElementAction.getElementFromListByText(elements, "Leads");
        ElementAction.clickOn(element);
    }

    public boolean isLeadsBtnDisplayed() {
        return ElementAction.isDisplayed(leadsBtnLink, Selectors.xpath);
    }

    public boolean isViewProfileImgDisplayed() { return ElementAction.isDisplayed(viewProfile_xpath, Selectors.xpath); }

    private void clickOnViewProfile() { ElementAction.clickWithKeyBord(viewProfile_xpath, Selectors.xpath); }

    private boolean isAccountOwnerNameDisplayed() { return ElementAction.isDisplayed(accountOwnerName_xpath, Selectors.xpath); }

    private void clickOnLogOut() { ElementAction.clickWithJavaScriptOnStaleElement(logOutLink_xpath, Selectors.xpath); }

    public void clickOnTheLogOutBtn() {
        clickOnViewProfile();
        clickOnLogOut();
    }

    public void closeAllOpenTabs() {
        try {
            if (ElementAction.isElementPresent(closeTabBtn_xpath, Selectors.xpath)) {
                List<WebElement> elements = ElementAction.getWebElements(closeTabBtn_xpath, Selectors.xpath);
                logger.info("Total Open Tabs Are: " + elements.size());
                for (int i = 0; i < elements.size(); i++) {
                    ElementAction.clickOn(elements.get(i));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Not able to close all open tab !!");
        }
    }
}
