package com.coa.qa.pages.webelements;

public interface HomePageElements {
    String username_xpath = "//input[@type='email']";
    String password_xpath = "//input[@type='password']";
    String loginBtn_xpath = "//input[@id='Login']";
    String homeBtnLink_xpath = "//a[@title = 'Home']";
    String reminderAdpLoginBtn_xpath = "//button[text() = 'Finish']";
    String salesforceLogo_xpath = "//img[@class = 'standard_logo']";
}
