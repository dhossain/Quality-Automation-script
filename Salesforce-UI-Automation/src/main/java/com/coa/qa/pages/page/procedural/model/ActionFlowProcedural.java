package com.coa.qa.pages.page.procedural.model;

import com.coa.qa.commons.*;
import com.coa.qa.pages.pageobject.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ActionFlowProcedural {
    private static final Logger logger = LogManager.getLogger(ActionFlowProcedural.class);
    private CommonActionFlowObject commonActionFlowObject = new CommonActionFlowObject();
    private OpportunityAddDebtObject opportunityAddDebtObject = new OpportunityAddDebtObject();
    private OpportunityActionsApplicantInfoObject opportunityActionsApplicantInfoObject = new OpportunityActionsApplicantInfoObject();
    private OpportunityActionIncomeExpensesObject opportunityActionIncomeExpensesObject = new OpportunityActionIncomeExpensesObject();
    private ProgramSummaryObject programSummaryObject = new ProgramSummaryObject();
    private DraftFrequencyObject draftFrequencyObject = new DraftFrequencyObject();
    private ClintAndBankValidationObject clintAndBankValidationObject = new ClintAndBankValidationObject();
    private OpportunityPageObject opportunityPageObject = new OpportunityPageObject();
    private FinalApprovalActionsObject finalApprovalActionsObject = new FinalApprovalActionsObject();
    private UnderwriterHomePageObject underwriterHomePageObject = new UnderwriterHomePageObject();
    private ApprovalQueuePageObject approvalQueuePageObject = new ApprovalQueuePageObject();
    private LeadsPageObject leadsPageObject = new LeadsPageObject();

    public boolean customerInfoVerify() {
        logger.info("Entering and Verifying Customer Information Info");
        if (opportunityActionsApplicantInfoObject.getApplicantFirstName() == null
                || opportunityActionsApplicantInfoObject.getApplicantLastName() == null
                || opportunityActionsApplicantInfoObject.getApplicantEmail() == null
                || opportunityActionsApplicantInfoObject.getApplicantBirthDate() == null
                || opportunityActionsApplicantInfoObject.getApplicantStreetAddress() ==null
                || opportunityActionsApplicantInfoObject.getApplicantStateAddress() == null
                || opportunityActionsApplicantInfoObject.getApplicantCityAddress() == null
                || opportunityActionsApplicantInfoObject.getApplicantZipcode() == null
                || opportunityActionsApplicantInfoObject.getApplicantMobileNumber() == null) {
            logger.error("Customer Information Fields are NOT pre-populated");
            return false;
        } else {
            logger.info("Customer Information Fields are pre-populated");
            opportunityActionsApplicantInfoObject.enterApplicantSSN("132-53-7824");
            return true;
        }
    }

    public boolean isCustomerInfoCompleted() {
        logger.info("Agent is landing on the Client Employer Details page");
        commonActionFlowObject.clickOnNextBtnInAction();
        return opportunityActionsApplicantInfoObject.isPrimaryContractNameDisplayed();
    }

    public boolean isClientBudgetCompleted() {
        logger.info("Entering Client Employer Info");
        opportunityActionsApplicantInfoObject.selectClientEmployerIndustry();
        opportunityActionsApplicantInfoObject.clickOnNextIncomeBtn();
        return opportunityActionIncomeExpensesObject.isApplicantNetSalaryTextDisplayed();
    }

    public boolean completeClint_CoClintEmployerIndustry() {
        opportunityActionsApplicantInfoObject.selectCoClientEmployerIndustry();
        opportunityActionsApplicantInfoObject.selectClientEmployerIndustry();
        opportunityActionsApplicantInfoObject.clickOnNextIncomeBtn();
        return opportunityActionIncomeExpensesObject.isApplicantNetSalaryTextDisplayed();
    }

    public boolean customerInfoAndBudgetVerify() {
        try {
            logger.info("Complete Customer Information in Action Flow: " + customerInfoVerify());
            logger.info("Agent Land on the Budget Page: " + isCustomerInfoCompleted());
            return isClientBudgetCompleted();
        } catch (Exception ex) {
            logger.error("Not able to land on the Income Page ", ex);
            return false;
        }
    }

    private boolean isOpportunityHaveDebt() throws InterruptedException {
        Thread.sleep(3000);
        WebUtils.pageReload();
        return commonActionFlowObject.isDebtTableDisplayed();
    }

    public boolean addDebtInAction() {
        try{
            logger.info("Adding Debt in the Opportunity Action Flow");
            commonActionFlowObject.clickOnAddDebt();
            opportunityAddDebtObject.enterVillasAtChaseRelatedCreditor(RelatedCreditor.ChaseBank.VALUE);
            opportunityAddDebtObject.enterAccountNumber(Utils.randomNumber(10));
            opportunityAddDebtObject.selectIndividualAccountDesignator();
            opportunityAddDebtObject.selectApplicantDebtOwner();
            opportunityAddDebtObject.select120DaysDelinquent();
            opportunityAddDebtObject.enterDebtBalance("15000");
            opportunityAddDebtObject.selectDebtType();
            opportunityAddDebtObject.selectLegalOrCollection();
            commonActionFlowObject.clickOnNextBtnInAction();
            return isOpportunityHaveDebt();
        } catch (Exception ex) {
            logger.error("Debt Is NOT added: ", ex);
            return false;
        }
    }

    public String enterVerifyClintIncomeExpenses() {
        String successMessage = null;
        try {
            logger.info("Entering Clint Income and Expenses in the Action Flow");
            opportunityActionIncomeExpensesObject.enterApplicantNetSalary(Utils.randomNumber(100, 250));
            commonActionFlowObject.clickOnNextBtn();
            if (opportunityActionIncomeExpensesObject.isRentDisplayed()) {
                opportunityActionIncomeExpensesObject.enterApplicantRent(Utils.randomNumber(10, 99));
                commonActionFlowObject.clickOnNextBtn();
                if (opportunityActionIncomeExpensesObject.isHealthInsuranceDisplayed()) {
                    commonActionFlowObject.clickOnNextBtn();
                    if (opportunityActionIncomeExpensesObject.isCableInternetDisplayed()) {
                        commonActionFlowObject.clickOnNextBtn();
                        if (opportunityActionIncomeExpensesObject.isMarketValuePrimaryHouseDisplayed()) {
                            commonActionFlowObject.clickOnNextBtn();
                            if (opportunityActionIncomeExpensesObject.isPrimaryHomeMortgageBalanceDisplayed()) {
                                commonActionFlowObject.clickOnNextBtn();
                                if (opportunityActionIncomeExpensesObject.getTotalLivingCost() != null
                                        && opportunityActionIncomeExpensesObject.getTotalIncomeMinusExpenses() != null
                                        && opportunityActionIncomeExpensesObject.getTotalIncomeCombined() != null ) {
                                    commonActionFlowObject.clickOnNextBtn();
                                    successMessage = commonActionFlowObject.getIncomeExpenseSuccessMessage();
                                }
                            }
                        }
                    }
                }
            }
            return successMessage;
        }catch (Exception ex) {
            logger.error("Clint Income and Expenses is NOT completed ", ex);
            return null;
        }
    }

    public boolean enterVerifyProgramSummary(int numberSuccessCheckList) {
        try {
            int numberOfSuccessMark = 0;
            commonActionFlowObject.clickOnProgramSummary();
            programSummaryObject.enterNewTermLength("47");
            commonActionFlowObject.clickOnNextBtn();
            programSummaryObject.clickOnCloseBtn();
            commonActionFlowObject.clickOnNextBtnInAction();
            draftFrequencyObject.enterProgramStartDate(Utils.futureDate(10));
            draftFrequencyObject.selectDraftFrequencyMonthly();
            commonActionFlowObject.clickOnNextBtn();
            if (commonActionFlowObject.getPopulatedStartDate() != null
                    && commonActionFlowObject.getPopulatedDraftFrequency() != null) {
                Thread.sleep(1000);
                commonActionFlowObject.clickOnNextBtnInAction();
                numberOfSuccessMark = commonActionFlowObject.getNumberOfSuccessMark();
            }
            return numberOfSuccessMark == numberSuccessCheckList;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Program Summary is NOT created ", e);
            return false;
        }
    }

    public boolean clintAndBankValidation() {
        logger.info("Bank and Clint Validation start");
        try {
            return clintAndBankValidationObject.clintBankValidation(
                    BankingInfo.BankName.VALUE,
                    BankingInfo.ChaseBank_RoutingNumber.VALUE,
                    Utils.randomNumber(9));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Bank and Clint Validation are NOT done ", e);
            return false;
        }
    }

    public boolean submittedVerifyFinalApproval(String result) {
        logger.info("Submitting for Final Approval");
        try {
            boolean isAccountLocked = false;
            if ( opportunityPageObject.setStatusStageSubStage(
                    OpportunityStatus.Final_Approval.VALUE,
                    OpportunityStatus.Contract_Signed.VALUE)) {
                WebUtils.pageReload();
                isAccountLocked = finalApprovalActionsObject.finalApprovalSubmit(result);
            }
            return isAccountLocked;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Account is NOT Locked for Final Approval", e);
            return false;
        }

    }

    public String opportunityFinalization(String outcomeStatus, String opportunityName) {
        logger.info("Submitting for Final Approval from Underwriter Account");
        underwriterHomePageObject.clickOnFinalApprovalsTab();
        underwriterHomePageObject.clickOnRequestNumber(opportunityName);
        approvalQueuePageObject.clickOnTakeOwnership();
        approvalQueuePageObject.clickOnNextBtn();
        approvalQueuePageObject.selectOutcomeRequest(outcomeStatus);
        approvalQueuePageObject.clickOnNextBtn();
        approvalQueuePageObject.writeDecisionComments("Submitting for " + outcomeStatus);
        approvalQueuePageObject.clickOnNextBtn();
        return approvalQueuePageObject.getStatus();
    }

    public boolean stageValidationInApprovalQueue(String opportunityName, String stage, String subStage) {
        logger.info("Verifying Stage and Sub-Stage in the Approval Queue");
        boolean isStageSubStageValid = false;
        approvalQueuePageObject.clickOnDetailsTab();
        if (approvalQueuePageObject.getOpportunityName().equalsIgnoreCase(opportunityName)) {
            isStageSubStageValid =
                    approvalQueuePageObject.getOppStage().equalsIgnoreCase(stage)
                    && approvalQueuePageObject.getCurrentSubStage().equalsIgnoreCase(subStage);
        }
        return isStageSubStageValid;
    }

    public boolean editLeadWithCoApplicant(String firstName, String lastName) {
        logger.info("Entering Co-Applicant Information in the Lead Page");
        leadsPageObject.clickOnEditCoApplicantBtn();
        leadsPageObject.enterCoApplicantFirstName(firstName);
        leadsPageObject.enterCoApplicantLastName(lastName);
        leadsPageObject.enterCoApplicantMobileNo(Utils.randomNumber(9));
        leadsPageObject.enterCoApplicantEmail(
                com.clearone.commonUtils.Utils.getEmail(firstName, lastName));
        leadsPageObject.enterCoApplicantSsn("123-78-9342");
        leadsPageObject.enterCoApplicantBirthdate(Utils.setDate(20));
//        leadsPageObject.clickOnAddressSameAsApplicantIcon();
        commonActionFlowObject.clickOnSaveBtn();
        return leadsPageObject.getCoApplicantLastName().equals(lastName);
    }

    public boolean applicantCoApplicantVerifyInAction() {
        if (customerInfoVerify()) {
            if (opportunityActionsApplicantInfoObject.getCoApplicantFirstName() == null
                    || opportunityActionsApplicantInfoObject.getCoApplicantLastName() == null
                    //|| opportunityActionsApplicantInfoObject.getCoApplicantEmail() == null
                    || opportunityActionsApplicantInfoObject.getCoApplicantSsn() == null
                    || opportunityActionsApplicantInfoObject.getApplicantBirthDate() == null ) {
                logger.error("C0-Applicant Information Fields are NOT pre-populated");
                return false;
            } else {
                opportunityActionsApplicantInfoObject.enterStreetAddress(com.clearone.commonUtils.Utils.getStreetAddress());
                opportunityActionsApplicantInfoObject.enterCity("TestCity");
                opportunityActionsApplicantInfoObject.selectState(2);
                opportunityActionsApplicantInfoObject.enterZipcode("12760");
                commonActionFlowObject.clickOnNextBtnInAction();
                return opportunityActionsApplicantInfoObject.isPrimaryContractNameDisplayed();
            }
        } else {
            return false;
        }
    }
}
