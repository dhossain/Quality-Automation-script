package com.coa.qa.pages.pageobject;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.actions.Selectors;
import com.coa.qa.pages.webelements.CommonActionFlowElements;
import io.cucumber.java.an.E;
import org.openqa.selenium.WebElement;

public class CommonActionFlowObject implements CommonActionFlowElements {
    public void clickOnAddDebt() {
        WebElement element = ElementAction.getElementByIndex(actionFlowTabsBtn_xpath, Selectors.xpath, 2);
        ElementAction.clickWithJavaScript(element);
    }

    public void clickOnProgramSummary() {
        WebElement element = ElementAction.getElementByIndex(actionFlowTabsBtn_xpath, Selectors.xpath, 3);
        ElementAction.clickWithJavaScript(element);
    }

    public void clickOnNextBtn() {
        ElementAction.clickWithJavaScript(nextIncomeBtn_xpath, Selectors.xpath);
    }

    public void clickOnNextBtnInAction() {
        ElementAction.clickWithJavaScript(actionFlowNextBtn_xpath, Selectors.xpath);
    }

    public void clickOnDebtsCreditReportsButton() {
        ElementAction.clickWithJavaScript(debtsCreditReportsBtn_xpath, Selectors.xpath);
    }

    public boolean isDebtTableDisplayed() {
        clickOnDebtsCreditReportsButton();
        return ElementAction.isDisplayed(debtsTable_xpath, Selectors.xpath);
    }

    public String getIncomeExpenseSuccessMessage() {
        String message_1 = ElementAction.getText(ElementAction.getElementByIndex(incomeExpensesCompletedMessages_xpath, Selectors.xpath, 0));
        String message_2 = ElementAction.getText(ElementAction.getElementByIndex(incomeExpensesCompletedMessages_xpath, Selectors.xpath, 1));
        return message_1 + " " + message_2;
    }

    public String getActionsMessage() {
        return ElementAction.getText(ElementAction.getElementByIndex(incomeExpensesCompletedMessages_xpath, Selectors.xpath, 0));
    }

    public int getNumberOfSuccessMark() {
      return ElementAction.getWebElements(listOfCheckmarkIcon_xpath, Selectors.xpath).size();
    }

    public String getPopulatedStartDate() { return ElementAction.getText(populatedStartDate_xpath, Selectors.xpath); }

    public boolean isStartDateDisplayed() { return ElementAction.isDisplayed(populatedStartDate_xpath, Selectors.xpath); }

    public String getPopulatedDraftFrequency() { return ElementAction.getText(populatedDraftFrequency_xpath, Selectors.xpath); }

    public void clickOnSaveBtn() { ElementAction.clickOn(saveBtn_xpath, Selectors.xpath); }

    public void clickOnNextBtn(int index) { ElementAction.clickWithJavaScript(ElementAction.getElementByIndex(listOfNextBtn_xpath, Selectors.xpath, index)); }

    public void clickOnTab(String tabName) {
        ElementAction.clickWithJavaScript(
                ElementAction.getElementFromListByText(
                        ElementAction.getWebElements(tabList_xpath, Selectors.xpath), tabName));
    }

    public String getProcessor() { return ElementAction.getText(processor_xpath, Selectors.xpath); }

    public String getCallScriptTask() { return ElementAction.getText(callScriptTask_xpath, Selectors.xpath); }
}
