package com.coa.qa.helper;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
    private static final Logger logger = LogManager.getLogger(BaseClass.class);
    private static String SET_BROWSER = null;
    private static final String CHROME_BROWSER = "chrome";
    private static final String EDGE_BROWSER = "edge";
    public static int PAGE_LOAD_TIMEOUT = 15;
    public static Properties config = null;
    public static WebDriver driver = null;

    private static void LoadConfigProperty(String environment) throws IOException {
        config = new Properties();
        String path = null;
        switch (environment.toLowerCase()) {
            case "stage":
                path = "/src/main/resources/config/config-stage.properties";
                break;

            case "prod":
                path = "/src/main/resources/config/config-prod.properties";
                break;

            case "uat":
                path = "/src/main/resources/config/config-uat.properties";
                break;
        }
        FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + path);
        config.load(ip);
        logger.info("Loading Environment: " + environment.toUpperCase());
    }

    private static void setDriverPath(String browser) throws IOException {
        String driverPath = null;
        if(System.getProperty("os.name").startsWith("Linux")) {
            switch (browser.toLowerCase()) {
                case EDGE_BROWSER:
                    //TODO
                    break;

                case CHROME_BROWSER:
                    driverPath = System.getProperty("user.dir") + "/src/main/resources/driver/linux/chromedriver.exe";
                    System.setProperty("webdriver.chrome.driver", driverPath);
                    break;
            }
        }
        if(System.getProperty("os.name").startsWith("Mac")) {
            switch (browser.toLowerCase()) {
                case EDGE_BROWSER:

                case CHROME_BROWSER:
                    //TODO
                    break;
            }
        }
        if(System.getProperty("os.name").startsWith("Windows")) {
            switch (browser.toLowerCase()) {
                case EDGE_BROWSER:
                    driverPath = System.getProperty("user.dir") + "/src/main/resources/driver/windows/msedgedriver.exe";
                    System.setProperty("webdriver.edge.driver", driverPath);
                    break;

                case CHROME_BROWSER:
                    driverPath = System.getProperty("user.dir") + "/src/main/resources/driver/windows/chromedriver.exe";
                    System.setProperty("webdriver.chrome.driver", driverPath);
                    break;
            }
        }
        logger.info("Driver Path: " + driverPath);
    }

    private static ChromeOptions browsePreSetUp(String browser) {
        ChromeOptions options = new ChromeOptions();
        switch (browser) {
            case "chrome":
                HashMap<String, Object> chromePrefs = new HashMap<>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                options.addArguments("--incognito");
                //options.addArguments("--headless");
                options.addArguments("window-size=1920,681");
                options.addArguments("disable-infobars");
                options.addArguments("--disable-gpu");
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-extensions");
                break;
        }
        return options;
    }

    private static void driverSetUp(String url) {
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.get(url);
        logger.info("URL Entered: " + url);
    }

    public static void driverInitialize(String browser, String environment){
        try {
            SET_BROWSER = browser;
            LoadConfigProperty(environment);
            setDriverPath(browser);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            switch (browser.toLowerCase()) {
                case EDGE_BROWSER:
                    driver = new EdgeDriver();
                    break;

                case CHROME_BROWSER:
                    driver = new ChromeDriver(browsePreSetUp(CHROME_BROWSER));
                    break;
            }
            logger.info("Browser is Opening: " + browser.toUpperCase());
            driverSetUp(config.getProperty("url"));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("NOT able to open the Browser: " + browser.toUpperCase());
        }
    }

    public static void browserReOpen() {
        logger.info("Browser Re-Opening: " + SET_BROWSER);
        switch (SET_BROWSER) {
            case CHROME_BROWSER:
                driver = new ChromeDriver(browsePreSetUp(CHROME_BROWSER));
                break;

            case EDGE_BROWSER:
                driver = new EdgeDriver();
                break;
        }
        driverSetUp(config.getProperty("url"));
    }

    public static void drawBorderOnTheElement(WebElement element) {
        JavascriptExecutor javascriptExecutor = ((JavascriptExecutor)driver);
        javascriptExecutor.executeScript("arguments[0].style.border='3px solid red'", element);
    }

    public static void drawGreenBorderOnTheElement(WebElement element) {
        JavascriptExecutor javascriptExecutor = ((JavascriptExecutor)driver);
        javascriptExecutor.executeScript("arguments[0].style.border='1px solid green'", element);
    }
}