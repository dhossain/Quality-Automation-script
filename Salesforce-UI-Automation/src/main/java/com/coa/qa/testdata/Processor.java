package com.coa.qa.testdata;

public enum Processor {
    ALPERSTEIN("Alperstein"),
    GREENBERG("Greenberg");

    public final String VALUE;
    Processor(String value) { this.VALUE = value; }
}
