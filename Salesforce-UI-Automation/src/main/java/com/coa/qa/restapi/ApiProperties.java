package com.coa.qa.restapi;

import com.clearone.commonUtils.Utils;
import com.coa.qa.helper.BaseClass;
import java.io.IOException;
import java.net.URISyntaxException;

public class ApiProperties extends BaseClass {
    public static String CREATE_LEAD_URL = null;
    public static String CREATE_LEAD_API_KEY = null;
    public static String CREATE_LEAD_JSON = null;

    public ApiProperties() {
        try {
            CREATE_LEAD_URL = config.getProperty("creatLeadUrl");
            CREATE_LEAD_API_KEY = config.getProperty("APIKEY");
            CREATE_LEAD_JSON = Utils.readFileFromResources("leadCreateTemplate.json");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
