package com.coa.qa.restapi;

import com.clearone.commonUtils.Headers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

public class HeaderProperties extends ApiProperties {

    protected Map<String, String> defaultHeaders() {
        Map <String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put(Headers.APIKEY.name(), super.CREATE_LEAD_API_KEY);
        return headers;
    }

    protected HttpHeaders restTemplateHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> map = new HashMap<>();
        map.put(Headers.APIKEY.name(), super.CREATE_LEAD_API_KEY);
        httpHeaders.setAll(map);
        return httpHeaders;
    }
}
