package com.coa.qa.restapi;

import com.clearone.restapi.RestApiCall;
import com.clearone.restapi.RestApiImpl;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public class CreateLeadModel {
    private static final Logger logger = LogManager.getLogger(com.clearone.model.CreateLeadModel.class);
    private RestApiCall restApiCall = new RestApiImpl();
    private HeaderProperties headerProperties = new HeaderProperties();
    private HttpHeaders httpHeaders = null;

    public ResponseEntity createLeadRestTemplate(String creatLeadUrl, String jsonBody) {
        try {
            ResponseEntity response = restApiCall.sendPostRequest(
                    jsonBody,
                    creatLeadUrl,
                    headerProperties.restTemplateHeaders());
            logger.info("Response Body: " + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Response createLead(String endpoint, String jsonBody) {
        return restApiCall.sendPostRequest(
                endpoint,
                headerProperties.defaultHeaders(),
                jsonBody);
    }
}
