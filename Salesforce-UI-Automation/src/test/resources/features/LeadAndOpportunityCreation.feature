Feature: Lead Creation and Conversion to Opportunity

  @Positive
  @Regression
  Scenario: Create a New Lead with Required Fields
    Given  User login in to the SalesForce as "SalesAgent"
    And User navigate to the Lead page
    And User click on the New Button to create a new lead
    When user do "Enter Required" fields and click on the SAVE button
    Then User should be able to create a new lead

  @Negative
  Scenario: Create a New Lead without Required Fields
    Given  User login in to the SalesForce as "SalesAgent"
    And User navigate to the Lead page
    And User click on the New Button to create a new lead
    When user do "NOT Enter Required" fields and click on the SAVE button
    Then User should be able to see an error message

  @Positive
  Scenario: Convert a lead to an opportunity
    Given  User login in to the SalesForce as "SalesManager"
    And User navigate to the Lead page
    And create a new lead with "Applicant"
    And User click on the Manager Conversion button
    And Enter Required information in the Manager Conversion process
    Then "Opportunity" account should be created

  @Positive
  Scenario: Verify Household, Person and Opportunity Account is created on Lead Conversion
    Given  User login in to the SalesForce as "SalesManager"
    And User navigate to the Lead page
    And create a new lead with "Co-Applicant"
    And User click on the Manager Conversion button
    And Enter Required information in the Manager Conversion process
    Then "Opportunity" account should be created
    And "Household" account should be created
    And "Applicant's Person" account should be created
    And "Co-Applicant's Person" account should be created

  @Regression
  @E2E
  @SF_1283
  Scenario: Final Approval process with Co-Applicant
    Given User login in to the SalesForce as "SalesManager"
    And Create a lead using LeadCreateAPI
    And Created "lead" open in the browser
    And Enter Co-Applicant information
    And Do Manager Conversion for lead conversion
    Then "Opportunity" account created
    And "Household" account created
    And "Applicant's Person" account created
    And "Co-Applicant's Person" account created
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "SalesAgent"
    And Created "Opportunity" open in the browser
    Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
    And Add budget
    When Completed and Verified applicant and co-applicant information
    And The opportunity is in "Qualification" stage and "Budget" sub-stage
    When Agent complete employer industry for Clint and Co-Clint
    And Agent completed client Income and Expenses in the Action Flow
    Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage
    When Agent completed Program Summary in the Action Flow
    Then The opportunity is in "Send Contract" stage and "Send Contract Validation" sub-stage
    When Bank validation and Clint validation are completed
    Then The opportunity is in "Send Contract" stage and "Send Contract" sub-stage
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "SystemAdmin"
    And Created "Opportunity" open in the browser
    Then Submit for Final Approval Account should be "Locked"
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "UnderWriter"
    Then UnderWriter should be able to "Approved"
    And The opportunity is "Enrollment" stage and "Contract Signed" sub-stage in the Approval Queue

  @Regression
  @SF_1298
  Scenario: Duplicate Lead Create Validation
    Given Create 1 duplicate lead using LeadCreateAPI
    And User login in to the SalesForce as "SalesAgent"
    When Created "lead" open in the browser
    Then There should be 1 duplicated lead record in the Duplicated Leads Queue
    And "Lead Transferred to inContact Successfully" Task should be created
    And Make Browser ready for re-login
    When User login in to the SalesForce as "SystemAdmin"
    And Created "lead" open in the browser
    Then inContract FileName should be populated

  @Regression
  @SF_1302
  Scenario: Lead Validation For Attorney Model State
    Given User login in to the SalesForce as "SalesManager"
    And Create a lead using LeadCreateAPI for "Attorney_Model_State"
    And Created "lead" open in the browser
    Then Verify Processor
    And Call Script should be displayed with ALPERSTEIN or GREENBERG CALL SCRIPT
    When Do Manager Conversion for lead conversion
    Then "Opportunity" account created
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "SalesAgent"
    And Created "Opportunity" open in the browser
    Then Opportunity Record type should be "Attorney Model"
    And Call Script should be displayed with ALPERSTEIN or GREENBERG CALL SCRIPT
