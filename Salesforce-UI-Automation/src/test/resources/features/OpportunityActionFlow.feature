Feature: Action Flow Process In The Opportunity

  Background: Create Lead For Action Flow Test
    Given User login in to the SalesForce as "SalesManager"
    And Create a lead using LeadCreateAPI
    And Created "lead" open in the browser
    And Do Manager Conversion for lead conversion
    Then "Opportunity" account created
    And "Household" account created
    And Make Browser ready for re-login

    @SF_1231
    Scenario: Action Flow: Customer Info Verification
      Given User login in to the SalesForce as "SalesAgent"
      And Created "Opportunity" open in the browser
      Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
      When Agent verify Customer Information in the Action Flow
      Then Agent should land on the Customer Job Details Page
      And The opportunity is in "Qualification" stage and "Budget" sub-stage
      And Agent completed client Job Details, should land on the Income page

    @SF_1247
    Scenario: Action Flow: Verification of Pre-Approval Stage in the Action Flow
      Given User login in to the SalesForce as "SalesAgent"
      And Created "Opportunity" open in the browser
      Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
      And Agent completed Customer Information and Budget in the Action Flow
      And The opportunity is in "Qualification" stage and "Budget" sub-stage
      When Agent completed client Income and Expenses in the Action Flow
      Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage

    @SF_1249
    Scenario: Action Flow: Verification of Send Contract Stage in the Action Flow
      Given User login in to the SalesForce as "SalesAgent"
      And Created "Opportunity" open in the browser
      Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
      And Agent completed Customer Information and Budget in the Action Flow
      And The opportunity is in "Qualification" stage and "Budget" sub-stage
      When Agent completed client Income and Expenses in the Action Flow
      Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage
      When Agent completed Program Summary in the Action Flow
      Then The opportunity is in "Send Contract" stage and "Send Contract Validation" sub-stage

    @SF_1255
    Scenario: Action Flow: Verification of Bank and Clint in the Action Flow
      Given User login in to the SalesForce as "SalesAgent"
      And Created "Opportunity" open in the browser
      Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
      And Agent completed Customer Information and Budget in the Action Flow
      And The opportunity is in "Qualification" stage and "Budget" sub-stage
      When Agent completed client Income and Expenses in the Action Flow
      Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage
      When Agent completed Program Summary in the Action Flow
      Then The opportunity is in "Send Contract" stage and "Send Contract Validation" sub-stage
      When Bank validation and Clint validation are completed
      Then The opportunity is in "Send Contract" stage and "Send Contract" sub-stage

    @SF_1255
    Scenario: Action Flow: Submit for Final Approval
      Given User login in to the SalesForce as "SalesAgent"
      And Created "Opportunity" open in the browser
      Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
      And Agent completed Customer Information and Budget in the Action Flow
      And The opportunity is in "Qualification" stage and "Budget" sub-stage
      When Agent completed client Income and Expenses in the Action Flow
      Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage
      When Agent completed Program Summary in the Action Flow
      Then The opportunity is in "Send Contract" stage and "Send Contract Validation" sub-stage
      When Bank validation and Clint validation are completed
      Then The opportunity is in "Send Contract" stage and "Send Contract" sub-stage
      And Make Browser ready for re-login
      Given User login in to the SalesForce as "SystemAdmin"
      And Created "Opportunity" open in the browser
      Then Submit for Final Approval Account should be "Locked"

  @Smoke
  @Regression
  @E2E
  @SF_1260
  Scenario: Action Flow: Final Approval Approved
    Given User login in to the SalesForce as "SalesAgent"
    And Created "Opportunity" open in the browser
    Then The opportunity is in "Qualification" stage and "Candidate Information" sub-stage
    And Agent completed Customer Information and Budget in the Action Flow
    And The opportunity is in "Qualification" stage and "Budget" sub-stage
    When Agent completed client Income and Expenses in the Action Flow
    Then The opportunity is in "Pre-Approval" stage and "Waiting to submit for Pre-Approval" sub-stage
    When Agent completed Program Summary in the Action Flow
    Then The opportunity is in "Send Contract" stage and "Send Contract Validation" sub-stage
    When Bank validation and Clint validation are completed
    Then The opportunity is in "Send Contract" stage and "Send Contract" sub-stage
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "SystemAdmin"
    And Created "Opportunity" open in the browser
    Then Submit for Final Approval Account should be "Locked"
    And Make Browser ready for re-login
    Given User login in to the SalesForce as "UnderWriter"
    Then UnderWriter should be able to "Approved"
    And The opportunity is "Enrollment" stage and "Contract Signed" sub-stage in the Approval Queue