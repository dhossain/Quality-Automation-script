package com.coa.qa.helper;

import com.coa.qa.commons.WebUtils;
import io.cucumber.java.Scenario;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class TestUtils extends BaseClass{
    private static final Logger logger = LogManager.getLogger(TestUtils.class);
    public static void takeScreenshot(Scenario scenario) {
        try {
            WebUtils.threadWait(2);
            TakesScreenshot ts = (TakesScreenshot) driver;
            byte[] screenshot = ts.getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName().concat(".png"));
            logger.info("Screenshot was taken !!!");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Not able to take screenshot !!!");
        }
    }
}
