package com.coa.qa.helper;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {
    private static final Logger logger = LogManager.getLogger(RetryAnalyzer.class);

    private static boolean isRerun = false;
    private int tryCount = 0;
    private final int maxTryCount = 3;

    @Override
    public boolean retry(ITestResult result) {
        logger.info("Retry Mechanism is Called");
        if(tryCount < maxTryCount) {
            tryCount++;
            return true;
        }
        return false;
    }

    public boolean isRerun() {
        return isRerun;
    }
}
