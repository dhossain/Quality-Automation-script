package com.coa.qa.helper;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CustomListener implements ITestListener, IAnnotationTransformer {
    private static final Logger logger = LogManager.getLogger(CustomListener.class);
    private List<ITestResult> failedTestsToRemove = new ArrayList<>();
    @Override
    public void onTestStart(ITestResult iTestResult) {
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        try {
            logger.info("Scenario Is Failed: " + iTestResult.getTestName());
            RetryAnalyzer retryAnalyzer = (RetryAnalyzer) iTestResult.getMethod().getRetryAnalyzer();
            if (retryAnalyzer.isRerun()) {
                failedTestsToRemove.add(iTestResult);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        for (ITestResult result : failedTestsToRemove) {
            iTestContext.getFailedTests().removeResult(result);
        }
    }

    @Override
    public void transform(ITestAnnotation iTestAnnotation, Class testClass, Constructor testConstructor, Method testMethod) {
        //TODO Uncomment this to activate Re-Run Mechanism
//        IRetryAnalyzer retryAnalyzer = iTestAnnotation.getRetryAnalyzer();
//        if (retryAnalyzer == null) {
//            iTestAnnotation.setRetryAnalyzer(RetryAnalyzer.class);
//        }
    }
}
