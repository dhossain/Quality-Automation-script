package com.coa.qa.helper;

import com.coa.qa.testdata.CoaStates;
import com.coa.qa.testdata.Processor;

import java.util.stream.IntStream;

public class VerificationUtils implements CoaStates {

    public boolean verifyProcessor(String state, String processor) {
        try {
            boolean isTrue = false;
            if (IntStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8).anyMatch(i -> attorneyModelStates[i].equalsIgnoreCase(state))
                    && processor.equalsIgnoreCase(Processor.GREENBERG.VALUE)) {
                isTrue = true;
            } else if (attorneyModelStates[9].equalsIgnoreCase(state)
                    && processor.equalsIgnoreCase(Processor.ALPERSTEIN.VALUE)){
                isTrue = true;
            }
            return isTrue;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
