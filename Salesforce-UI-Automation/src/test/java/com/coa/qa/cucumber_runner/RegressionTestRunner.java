package com.coa.qa.cucumber_runner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.coa.qa.stepDefinition"},
        tags = "@Regression",
        plugin = {"pretty",
                "json:target/json-reports/CucumberTestReport.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
)
public class RegressionTestRunner extends TestNGRunnerConfigure {}
