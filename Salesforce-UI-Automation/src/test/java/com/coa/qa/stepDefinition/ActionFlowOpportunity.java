package com.coa.qa.stepDefinition;

import com.clearone.commonUtils.Utils;
import com.coa.qa.actions.ElementAction;
import com.coa.qa.commons.ApiUtils;
import com.coa.qa.commons.MarketingVendor;
import com.coa.qa.commons.WebUtils;
import com.coa.qa.helper.BaseClass;
import com.coa.qa.helper.TestData;
import com.coa.qa.helper.TestUtils;
import com.coa.qa.helper.VerificationUtils;
import com.coa.qa.pages.*;
import com.coa.qa.restapi.LeadCreateAPI;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_scouse.An;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class ActionFlowOpportunity {
    private static final Logger logger = LogManager.getLogger(ActionFlowOpportunity.class);
    private ActionFlow actionFlow = new ActionFlow();
    private LeadsPageActions leadsPageActions = new LeadsPageActions();
    private OpportunityPageAction opportunityPageAction = new OpportunityPageAction();
    private LeadCreateAPI leadCreateAPI = new LeadCreateAPI();
    private ApiUtils apiUtils = new ApiUtils();
    private CommonPageAction commonPageAction = new CommonPageAction();
    private VerificationUtils verificationUtils = new VerificationUtils();

    public Scenario scenario;
    private String FIRST_NAME = Utils.getFirstName();
    private String LAST_NAME = Utils.getLastName();
    private String CO_APPLICANT_FIRST_NAME = Utils.getFirstName();
    private String CO_APPLICANT_LAST_NAME = Utils.getFirstName();
    private String LEAD_URL = null;
    private String OPPORTUNITY_URL = null;
    private String OPPORTUNITY_NAME = null;
    private String STATE_NAME = null;

    @Before
    public void setUp(Scenario scenario) {
        this.scenario = scenario;
    }

    @And("Create a lead using LeadCreateAPI")
    public void createLeadUsingApi() {
        try {
            LEAD_URL = leadCreateAPI.leadCreateWithRestAssured(FIRST_NAME, LAST_NAME);
            scenario.log("Created Lead using API: " + LEAD_URL);
        } catch (AssertionError | ParseException ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail("Lead Is NOT Created Using API !!!");
        }
    }

    @Given("Create {int} duplicate lead using LeadCreateAPI")
    public void createDuplicateLead(int numberOfDuplicateLeads) {
        try {
            List<Response> duplicate_leads_response = new ArrayList<>();
            String requestBody = apiUtils.setLeadCreateRequestBody(
                    FIRST_NAME,
                    LAST_NAME,
                    15000.00,
                    null,
                    MarketingVendor.TestMarketingVendorId.VALUE);
            Response original_lead_response = leadCreateAPI.leadCreateWithRestAssured(requestBody);
            LEAD_URL = apiUtils.getLeadUrl(original_lead_response.getBody().asString());
            Assert.assertEquals(original_lead_response.statusCode(), 200,
                    "Lead Is NOT created Through API");

            for (int index = 0; index < numberOfDuplicateLeads; index ++) {
                duplicate_leads_response.add(leadCreateAPI.leadCreateWithRestAssured(requestBody));
            }

            scenario.log("Created Lead using API: " + LEAD_URL);
            scenario.log("Number Of Created Duplicated Lead: " + duplicate_leads_response.size());

            Assert.assertEquals(duplicate_leads_response.size(), numberOfDuplicateLeads,
                    "Duplicate leads are NOT the same as Expected Duplicated Leads");
        } catch (AssertionError | ParseException ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail("Lead Is NOT Created Using API !!!");
        }
    }

    @And("Create a lead using LeadCreateAPI for {string}")
    public void create_lead_using_lead_create_API(String requestFor) {
        try {
            Response response = null;
            switch (requestFor) {
                case "Attorney_Model_State":
                    STATE_NAME = TestData.getAttorneyModelState();
                    response = leadCreateAPI.leadCreateWithRestAssured(
                            apiUtils.setLeadCreateRequestBody(
                                    FIRST_NAME,
                                    LAST_NAME,
                                    "15000.00",
                                    STATE_NAME));
                    break;
            }
            Assert.assertEquals(response.statusCode(), 200,
                    "Lead Is NOT created Through API");

            LEAD_URL = apiUtils.getLeadUrl(response.getBody().asString());
            scenario.log("Created Lead using API: " + LEAD_URL);
        } catch (AssertionError | ParseException ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail("Lead Is NOT Created Using API !!!");
        }
    }

    @Then("There should be {int} duplicated lead record in the Duplicated Leads Queue")
    public void duplicate_lead_validation_in_the_duplicate_lead_queue(int numberOfDuplicateLeads) {
        try {
            List <String> duplicateLeadName = leadsPageActions.getDuplicateLeadsName();
            scenario.log("Number of Duplicate Leads In The Duplicate Queue: " + duplicateLeadName.size());
            scenario.log("Duplicate Lead Name: " + duplicateLeadName);
            Assert.assertEquals(
                    duplicateLeadName.size(),
                    numberOfDuplicateLeads,
                    "Duplicate Lead number is NOT same as Original Lead Number");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("{string} Task should be created")
    public void created_task_validation(String taskName) {
        try {
            String actualCreatedTaskName = leadsPageActions.getCreatedTask(taskName);
            scenario.log("Created Task: " + actualCreatedTaskName);
            Assert.assertEquals(
                    actualCreatedTaskName,
                    taskName,
                    "Task Did NOT Created");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("inContract FileName should be populated")
    public void lead_sent_to_inContract() {
        try {
            boolean isSentToInContract = leadsPageActions.isInContractFileCreated();
            scenario.log("Is Lead Sent To In-Contract: " + isSentToInContract);
            Assert.assertTrue(isSentToInContract, "Lead Is NOT Sent To In-Contract");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("The opportunity is in {string} stage and {string} sub-stage")
    public void opportunityStageSubStageValidation(String stage, String subStage) {
        try {
            String stageActual = opportunityPageAction.getStage();
            String subStageActual = opportunityPageAction.getSubStage();
            scenario.log("Stage Value: " + stageActual);
            scenario.log("Sub-Stage Value: " +subStageActual);
            Assert.assertEquals(stageActual, stage);
            Assert.assertEquals(subStageActual, subStage);
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Add budget")
    public void addBudget() {
        try {
            boolean isDebtAdded = actionFlow.addDebt();
            WebUtils.pageReload();
            scenario.log("Is Customer Debt Info Added: " + isDebtAdded);
            Assert.assertTrue(isDebtAdded, "Customer Debt is NOT Added");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Completed and Verified applicant and co-applicant information")
    public void applicant_and_CoApplicant_Verify() {
        try {
            boolean isInfoVerified = actionFlow.isApplicantCoApplicantInfoVerifiedInAction();
            scenario.log("Is Applicant Co-Applicant Information Verified In The Action Flow: " + isInfoVerified);
            Assert.assertTrue(isInfoVerified, "Applicant Co-Applicant Information is NOT Verified In The Action Flow");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @When("Agent complete employer industry for Clint and Co-Clint")
    public void complete_employer_industry_for_clint_and_CoClint() {
        try {
            boolean isEmployerIndustryCompleted = actionFlow.isClintCoClintEmployerIndustryCompleted();
            scenario.log("Is Employer Industry Completed For Clint And Co-Clint: " + isEmployerIndustryCompleted);
            Assert.assertTrue(isEmployerIndustryCompleted, "Employer Industry is NOT Completed For Clint And Co-Clint");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Agent completed client Job Details, should land on the Income page")
    public void completedClientJobDetails() {
        try {
            boolean isClientJobDetailsCompleted = actionFlow.isClientBudgetCompleted();
            scenario.log("Is Client Job Details Completed: " + isClientJobDetailsCompleted);
            Assert.assertTrue(isClientJobDetailsCompleted, "Client Job Details is NOT Completed");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Agent verify Customer Information in the Action Flow")
    public void verifyCustomerInfo() {
        try {
            boolean isCustomerInfoVerified = actionFlow.enterAndVerifyCustomerInfo();
            scenario.log("Is Customer Info Verified: " + isCustomerInfoVerified);
            Assert.assertTrue(isCustomerInfoVerified, "Customer Information is NOT verified in the Action Flow");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Agent completed Customer Information and Budget in the Action Flow")
    public void verifyCustomerInfoAndBudget() {
        try {
            boolean isDebtAdded = actionFlow.addDebt();
            WebUtils.pageReload();
            scenario.log("Is Customer Debt Info Added: " + isDebtAdded);
            Assert.assertTrue(isDebtAdded, "Customer Debt is NOT Added");
            TestUtils.takeScreenshot(scenario);

            boolean isCustomerInfoBudgetCompleted = actionFlow.completeCustomerInfoAndBudget();
            scenario.log("Is Customer Info and Budget Added: " + isCustomerInfoBudgetCompleted);
            Assert.assertTrue(isCustomerInfoBudgetCompleted, "Customer Info and Budget is NOT Completed");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Agent completed client Income and Expenses in the Action Flow")
    public void verifyCustomerIncomeAndExpenses() {
        try {
            String incomeCompletedMsg = actionFlow.enterClintIncomeExpenses();
            scenario.log("Income and Expenses completed message: " + incomeCompletedMsg);
            Assert.assertNotNull(incomeCompletedMsg, "Income and Expenses section is NOT completed");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @When("Agent completed Program Summary in the Action Flow")
    public void completeProgramSummary() {
        try {
            int numberSuccessCheckList = 10;
            boolean isProgramSummaryCompleted = actionFlow.completeProgramSummary(numberSuccessCheckList);
            scenario.log("Is Program Summary Completed: " + isProgramSummaryCompleted);
            Assert.assertTrue(isProgramSummaryCompleted, "Program Summary is NOT Completed.");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @When("Bank validation and Clint validation are completed")
    public void clintAndBankValidation() {
        try {
            boolean isBankClintValidationCompleted = actionFlow.clintAndBankValidationComplete();
            scenario.log("Are Bank and Clint Validation Completed: " + isBankClintValidationCompleted);
            Assert.assertTrue(isBankClintValidationCompleted, "Bank and Clint Validation are NOT Completed.");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Agent should land on the Customer Job Details Page")
    public void shouldLandOnActionPage() {
        try {
            boolean isAgentLanded = actionFlow.isCustomerInfoCompleted();
            scenario.log("Is Agent Land on the Action: " + isAgentLanded);
            Assert.assertTrue(isAgentLanded, "Agent NOT able to land on the Action page");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Created {string} open in the browser")
    public void openAccountInBrowser(String accountType) {
        try {
            switch (accountType.toLowerCase()) {
                case "lead":
                    WebUtils.enterUrl(LEAD_URL);
                    scenario.log("Lead Opened In The Browser: " + LEAD_URL);
                    break;

                case "opportunity":
                    WebUtils.enterUrl(OPPORTUNITY_URL);
                    scenario.log("Opportunity Opened In The Browser: " + OPPORTUNITY_URL);
                    break;
            }
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail("NOT able to open in the browser !!!");
        }
    }

    @Then("Verify Processor")
    public void verify_processor() {
        try {
            String processor = commonPageAction.getProcessor(STATE_NAME);
            scenario.log("State: " + STATE_NAME);
            scenario.log("Processor: " + processor);
            Assert.assertTrue(verificationUtils.verifyProcessor(STATE_NAME, processor),
                    "Processor verification is Failed");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Call Script should be displayed with ALPERSTEIN or GREENBERG CALL SCRIPT")
    public void call_script_should_have_ALPERSTEIN_or_GREENBERG() {
        try {
            boolean isScriptCreated = commonPageAction.IsCallScriptPopulated(STATE_NAME);
            scenario.log("State: " + STATE_NAME);
            scenario.log("Is Call Script Task Created: " + isScriptCreated);
            Assert.assertTrue(isScriptCreated, "Call Script Task is NOT Created");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @Then("Opportunity Record type should be {string}")
    public void opportunity_record_type_validation(String opportunityType) {
        try {
            String actualOpportunityType = opportunityPageAction.getOpportunityType();
            scenario.log("Opportunity Type: " + actualOpportunityType);
            Assert.assertEquals(actualOpportunityType, opportunityType,
                    "Opportunity TYpe Is NOT Matching");
            TestUtils.takeScreenshot(scenario);

        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Enter Co-Applicant information")
    public void enterCoApplicantInfo() {
        try {
            boolean isCoApplicantEntered = actionFlow
                    .isCoApplicantEntered(CO_APPLICANT_FIRST_NAME, CO_APPLICANT_LAST_NAME);
            scenario.log("Is Co-Applicant Entered: " + isCoApplicantEntered);
            Assert.assertTrue(isCoApplicantEntered, "Co-Applicant is NOT Entered");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Do Manager Conversion for lead conversion")
    public void managerConversion() {
        try {
            leadsPageActions.clickManagerConversion();
            boolean isLeadConverted = leadsPageActions.managerConversion(false);
            scenario.log("Is Lead Converted: " + isLeadConverted);
            Assert.assertTrue(isLeadConverted, "Lead is NOT converted to opportunity");
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @Then("{string} account created")
    public void opportunityCreated(String accountType) {
        try {
            String accountName = null;
            switch (accountType.toLowerCase()) {
                case "opportunity":
                    accountName = leadsPageActions.getOpportunityTitleName();
                    OPPORTUNITY_NAME = accountName;
                    Assert.assertTrue(
                            accountName.contains(FIRST_NAME + " " + LAST_NAME),
                            "Opportunity Does NOT Contain Applicant First & Last name");
                    break;

                case "household":
                    accountName = opportunityPageAction.getHouseholdName();
                    Assert.assertTrue(
                            accountName.contains(LAST_NAME),
                            "Household Does NOT Contain Applicant Last name");
                    break;

                case "applicant's person":
                    accountName = opportunityPageAction.getApplicantAccountName();
                    Assert.assertTrue(
                            accountName.contains(FIRST_NAME +  " " + LAST_NAME),
                            "Applicant Person Account Does NOT Contain Applicant Full name");
                    break;

                case "co-applicant's person":
                    accountName = opportunityPageAction.getCoApplicantAccountName();
                    Assert.assertTrue(
                            accountName.contains(CO_APPLICANT_FIRST_NAME + " " + CO_APPLICANT_LAST_NAME),
                            "Co-Applicant Person Account Does NOT Contain Co-Applicant First & Last name");
                    break;
            }
            scenario.log(accountType + " Account Name: " + accountName);
            Assert.assertNotNull(accountName, "Opportunity Name are NULL");
            OPPORTUNITY_URL = WebUtils.getCurrentUrl();
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError | InterruptedException ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @Then("UnderWriter should be able to {string}")
    public void finalApprovalStep(String status) {
        try {
            boolean isTrue = actionFlow.opportunityFinalApproved(OPPORTUNITY_NAME, status);
            scenario.log("Is The Opportunity Finally " + status + ": " + isTrue);
            Assert.assertTrue(isTrue, "UnderWriter NOT able to " + status);
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("The opportunity is {string} stage and {string} sub-stage in the Approval Queue")
    public void stage_SubStage_Validation_In_Approval_Queue(String stage, String subStage) {
        try {
            boolean isStageSubStageValidate = actionFlow.stageSubStageValidationInApprovalQueue(OPPORTUNITY_NAME, stage, subStage);
            scenario.log("Stage and Sub-Stage are valid: " + isStageSubStageValidate);
            Assert.assertTrue(isStageSubStageValidate, "Opportunity stage and sub-stage are not validate");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @And("Make Browser ready for re-login")
    public void reOpenBrowser() {
        try{
//            Assert.assertTrue(navigationPageAction.isLoggedOut(), "NOT LoggedOut !!!");
//            logger.info("Successfully LoggedOut !!!");
            ElementAction.closeBrowser();
            BaseClass.browserReOpen();
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }

    @Then("Submit for Final Approval Account should be {string}")
    public void submitForFinalApproval(String result) {
        try {
            boolean isLocked = actionFlow.submitForFinalApproval(result);
            scenario.log("Is Account Locked for Final Approval: " + isLocked);
            Assert.assertTrue(isLocked, "Account is NOT Locked for Final Approval");
            TestUtils.takeScreenshot(scenario);
        } catch (AssertionError ex) {
            ex.printStackTrace();
            logger.error("Exception: ", ex);
            Assert.fail();
        }
    }
}
