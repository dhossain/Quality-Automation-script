package com.coa.qa.stepDefinition;

import com.coa.qa.actions.ElementAction;
import com.coa.qa.helper.TestUtils;
import com.coa.qa.pages.NavigationPageAction;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Hokes {
    private static final Logger logger = LogManager.getLogger(LeadCreationToOpportunity.class);
    private NavigationPageAction navigationPageAction = new NavigationPageAction();

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            TestUtils.takeScreenshot(scenario);
        }
        navigationPageAction.isLoggedOut();
        ElementAction.closeBrowser();
        logger.info("+++++ Scenario Ended: " +scenario.getName() + " +++++\n");
    }

    @Before
    public void setUp(Scenario scenario) {
        logger.info("+++++ Scenario Started: " +scenario.getName() + " +++++");
    }
}
