package com.coa.qa.stepDefinition;

import com.clearone.commonUtils.Utils;
import com.coa.qa.commons.WebUtils;
import com.coa.qa.helper.BaseClass;
import com.coa.qa.helper.TestUtils;
import com.coa.qa.pages.HomePageAction;
import com.coa.qa.pages.LeadsPageActions;
import com.coa.qa.pages.NavigationPageAction;
import com.coa.qa.pages.OpportunityPageAction;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Assert;

public class LeadCreationToOpportunity extends BaseClass{
    public Scenario scenario;
    private static final Logger logger = LogManager.getLogger(LeadCreationToOpportunity.class);
    private HomePageAction homePageAction = new HomePageAction();
    private NavigationPageAction navigationPageAction = new NavigationPageAction();
    private LeadsPageActions leadsPageActions = new LeadsPageActions();
    private OpportunityPageAction opportunityPageAction = new OpportunityPageAction();

    private String FIRST_NAME = Utils.getFirstName();
    private String LAST_NAME = Utils.getLastName();
    private String CO_FIRST_NAME = Utils.getFirstName();
    private String CO_LAST_NAME = Utils.getLastName();

    @Before
    public void setUp(Scenario scenario) { this.scenario = scenario; }

    @Given("User login in to the SalesForce as {string}")
    public void userLogIn(String userRole) {
        try {
            switch (userRole) {
                case "SalesAgent":
                    homePageAction.login(config.getProperty("salesAgentUsername"), config.getProperty("salesAgentPassword"));
                    break;

                case "SalesManager":
                    homePageAction.login(config.getProperty("salesManagerUsername"), config.getProperty("salesManagerPassword"));
                    break;

                case "SystemAdmin":
                    homePageAction.login(config.getProperty("systemAdminUsername"), config.getProperty("systemAdminPassword"));
                    break;

                case "UnderWriter":
                    homePageAction.login(config.getProperty("underWriterUsername"), config.getProperty("underWriterPassword"));
                    break;
            }
            boolean isLoggedIn = homePageAction.isLoggedIn();
            scenario.log("User Role: " + userRole);
            scenario.log("Is User LoggedIn: " + isLoggedIn);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
            Assert.assertTrue(isLoggedIn, "User NOT able to logIn !!!");
            navigationPageAction.closeOpenedTabs();
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail("Not Able to Login !!!");
            logger.error("NOT able to login!!!");
        }
    }

    @And("User navigate to the Lead page")
    public void navigateToTheLeadPage() {
        try {
            navigationPageAction.navigateToLeadPage();
            boolean isNavigateToLead = navigationPageAction.isLeadBtnDisplayed();
            scenario.log("Is User Navigate to the Lead Page: " + isNavigateToLead);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
            Assert.assertTrue(isNavigateToLead, "User NOT able to Navigate to the Lead page !!!");
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("NOT able to navigate to the lead page !!!");
        }
    }

    @And("User click on the New Button to create a new lead")
    public void clickOnTheNewBtn() {
        try {
            leadsPageActions.clickOnTheNewBtn();
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("NOT able to click on NEW button!!!");
        }
    }

    @When("user do {string} fields and click on the SAVE button")
    public void enterRequiredFields(String str) {
        try {
            String lastName = LAST_NAME;
            String firstName = FIRST_NAME;
            String email = null;
            switch (str) {
                case "Enter Required":
                    email = Utils.getEmail(firstName,lastName);
                    leadsPageActions.enterRequireFields(lastName, email);
                    leadsPageActions.clickSaveBtn();
                    break;

                case "NOT Enter Required":
                    leadsPageActions.enterRequireFields(lastName, email);
                    leadsPageActions.clickSaveBtn();
                    break;
            }
            scenario.log("Customer LastName: " + lastName);
            scenario.log("Customer Email: " + email);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("NOT able to enter require fields !!!");
        }
    }

    @Then("User should be able to create a new lead")
    public void leadShouldCreated() {
        try {
            boolean isLeadCreated = leadsPageActions.isCustomerNameDisplayedOnLeadPage();
            String leadId = WebUtils.getLeadIdFromUrl();
            scenario.log("Is User created the Lead: " + isLeadCreated);
            scenario.log("Lead ID: " + leadId);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
            Assert.assertTrue(isLeadCreated);
            Assert.assertNotNull(leadId);
            TestUtils.takeScreenshot(scenario);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("Lead is NOT created !!!");
        }
    }

    @Then("User should be able to see an error message")
    public void leadNotCreatedErrMessage() {
        try {
            boolean isLeadCreated = leadsPageActions.isCustomerNameDisplayedOnLeadPage();
            String errMsg = leadsPageActions.getErrMsgLeadNotCreated();
            scenario.log("Is User created the Lead: " + isLeadCreated);
            scenario.log("Error Message: " + errMsg);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
            Assert.assertNotNull(errMsg, "Error message NOT shown !!!");
            TestUtils.takeScreenshot(scenario);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("Lead is created!!!");
        }
    }

    @And("create a new lead with {string}")
    public void createLead(String leadWith) {
        try {
            String lastName = LAST_NAME;
            String firstName = FIRST_NAME;
            String coApplicantLastName = CO_LAST_NAME;
            String coApplicantFirstName = CO_FIRST_NAME;
            String email = Utils.getEmail(firstName,lastName);
            boolean isLeadCreated = false;

            switch (leadWith.toLowerCase()) {
                case "applicant":
                    isLeadCreated = leadsPageActions.createLeadsWithMinimumInfo(firstName, lastName, email);
                    break;

                case "co-applicant":
                    isLeadCreated = leadsPageActions.createLeadsWithCoApplicant(firstName, lastName, coApplicantFirstName, coApplicantLastName);
                    scenario.log("Co-Applicant Name: " + coApplicantFirstName + " " + coApplicantLastName);
                    break;
            }

            scenario.log("Is Lead Created: " + isLeadCreated);
            scenario.log("Applicant Name: " + firstName + " " + lastName);
            scenario.log("Applicant Email: " + email);
            scenario.log("Current URL: " + WebUtils.getCurrentUrl());
            Assert.assertTrue(isLeadCreated, "Lead is NOT created");
            TestUtils.takeScreenshot(scenario);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.fail();
            logger.error("NOT able to create a new Lead!!!");
        }
    }

    @And("User click on the Manager Conversion button")
    public void clickOnManagerConversionBtn() {
        try {
            leadsPageActions.clickManagerConversion();
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("NOT able to click on Convert Button!!!");
            Assert.fail();
        }
    }

    @And("Enter Required information in the Manager Conversion process")
    public void managerConversionRequiredInfo() {
        try {
            boolean isLeadConverted = leadsPageActions.managerConversion(true);
            scenario.log("Is Lead Converted: " + isLeadConverted);
            Assert.assertTrue(isLeadConverted, "Lead is NOT converted to opportunity");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Lead is NOT converted to opportunity !!!");
            Assert.fail();
        }
    }

    @Then("{string} account should be created")
    public void opportunityCreated(String accountType) {
        try {
            String accountName = null;
            switch (accountType.toLowerCase()) {
                case "opportunity":
                    accountName = leadsPageActions.getOpportunityTitleName();
                    Assert.assertTrue(
                            accountName.contains(FIRST_NAME + " " + LAST_NAME),
                            "Opportunity Does NOT Contain Applicant First & Last name");
                    break;

                case "household":
                    accountName = opportunityPageAction.getHouseholdName();
                    Assert.assertTrue(
                            accountName.contains(LAST_NAME),
                            "Household Does NOT Contain Applicant Last name");
                    break;

                case "applicant's person":
                    accountName = opportunityPageAction.getApplicantAccountName();
                    Assert.assertTrue(
                            accountName.contains(FIRST_NAME + " Automation " + LAST_NAME),
                            "Applicant Person Account Does NOT Contain Applicant Full name");
                    break;

                case "co-applicant's person":
                    accountName = opportunityPageAction.getCoApplicantAccountName();
                    Assert.assertTrue(
                            accountName.contains(CO_FIRST_NAME + " " + CO_LAST_NAME),
                            "Co-Applicant Person Account Does NOT Contain Co-Applicant First & Last name");
                    break;
            }
            scenario.log(accountType + " Account Name: " + accountName);
            Assert.assertNotNull(accountName, "Opportunity Name are NULL");
            TestUtils.takeScreenshot(scenario);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Opportunity Name are NULL !!!");
            Assert.fail();
        }
    }
}