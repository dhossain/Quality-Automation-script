package com.coa.qa.pages;

import com.coa.qa.pages.page.procedural.model.ActionFlowProcedural;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ActionFlow {
    private static final Logger logger = LogManager.getLogger(ActionFlow.class);
    private ActionFlowProcedural actionFlowProcedural = new ActionFlowProcedural();

    public boolean addDebt() { return actionFlowProcedural.addDebtInAction(); }

    public boolean completeCustomerInfoAndBudget() { return actionFlowProcedural.customerInfoAndBudgetVerify(); }

    public boolean isClientBudgetCompleted() { return actionFlowProcedural.isClientBudgetCompleted(); }

    public boolean isCustomerInfoCompleted() { return actionFlowProcedural.isCustomerInfoCompleted(); }

    public boolean enterAndVerifyCustomerInfo() { return actionFlowProcedural.customerInfoVerify(); }

    public String enterClintIncomeExpenses() { return actionFlowProcedural.enterVerifyClintIncomeExpenses(); }

    public boolean completeProgramSummary(int numberSuccessCheckList) {
        return actionFlowProcedural.enterVerifyProgramSummary(numberSuccessCheckList);
    }

    public boolean clintAndBankValidationComplete() { return actionFlowProcedural.clintAndBankValidation(); }

    public boolean submitForFinalApproval(String result) { return actionFlowProcedural.submittedVerifyFinalApproval(result); }

    public boolean opportunityFinalApproved(String opportunityName, String status) {
        String actualStatus = null;
        switch (status) {
            case "Approved":
                actualStatus = actionFlowProcedural.opportunityFinalization("Approve", opportunityName);
                break;

            case "Rejected":
                actualStatus = actionFlowProcedural.opportunityFinalization("Reject", opportunityName);
                break;
        }
        return actualStatus.equalsIgnoreCase(status);
    }

    public boolean stageSubStageValidationInApprovalQueue(String opportunityName, String stage, String subStage){
        return actionFlowProcedural.stageValidationInApprovalQueue(opportunityName, stage, subStage);
    }

    public boolean isCoApplicantEntered(String firstName, String lastName) {
        return actionFlowProcedural.editLeadWithCoApplicant(firstName, lastName);
    }

    public boolean isApplicantCoApplicantInfoVerifiedInAction() {
        return actionFlowProcedural.applicantCoApplicantVerifyInAction();
    }

    public boolean isClintCoClintEmployerIndustryCompleted() {
        return actionFlowProcedural.completeClint_CoClintEmployerIndustry();
    }
}
