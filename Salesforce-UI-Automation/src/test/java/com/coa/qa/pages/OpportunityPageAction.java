package com.coa.qa.pages;

import com.coa.qa.pages.pageobject.OpportunityPageObject;

public class OpportunityPageAction {
    private OpportunityPageObject opportunityPageObject = new OpportunityPageObject();

    public String getHouseholdName() {
        return opportunityPageObject.getHouseholdAccountName();
    }

    public String getApplicantAccountName() {
        return opportunityPageObject.getApplicantAccountName();
    }

    public String getCoApplicantAccountName() {
        return opportunityPageObject.getCoApplicantAccountName();
    }

    public String getStage() { return opportunityPageObject.getStageValue(); }

    public String getSubStage() { return opportunityPageObject.getSubStageValue(); }

    public String getOpportunityType() { return opportunityPageObject.getOpportunityType(); }
}
