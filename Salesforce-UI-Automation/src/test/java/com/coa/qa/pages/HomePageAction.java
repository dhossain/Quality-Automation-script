package com.coa.qa.pages;

import com.coa.qa.pages.page.procedural.model.CommonProcedural;
import com.coa.qa.pages.pageobject.NavigationPageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class HomePageAction {
    private static final Logger logger = LogManager.getLogger(HomePageAction.class);
    private NavigationPageObject navigationPageObject = new NavigationPageObject();
    private CommonProcedural commonProcedural = new CommonProcedural();

    public void login(String userName, String password) {
        commonProcedural.logIn(userName, password);
    }

    public boolean isLoggedIn() { return navigationPageObject.isViewProfileImgDisplayed(); }
}
