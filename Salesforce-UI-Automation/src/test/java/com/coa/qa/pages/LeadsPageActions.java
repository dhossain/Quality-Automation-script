package com.coa.qa.pages;

import com.coa.qa.commons.Utils;
import com.coa.qa.commons.WebUtils;
import com.coa.qa.helper.BaseClass;
import com.coa.qa.pages.page.procedural.model.CommonProcedural;
import com.coa.qa.pages.page.procedural.model.LeadPageProcedural;
import com.coa.qa.pages.pageobject.LeadsPageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

public class LeadsPageActions {
    private static final Logger logger = LogManager.getLogger(LeadsPageActions.class);
    private LeadsPageObject leadsPageObject = new LeadsPageObject();
    private CommonProcedural commonProcedural = new CommonProcedural();
    private LeadPageProcedural leadPageProcedural = new LeadPageProcedural();

    public boolean createLeadsWithMinimumInfo(String firstName, String lastName, String email) {
        return commonProcedural.createLeads(firstName,lastName, email);
    }

    public boolean createLeadsWithCoApplicant(String applicantFirstName,
                                              String applicantLastName,
                                              String coApplicantFirstName,
                                              String coApplicantLastName) {
        return commonProcedural.createLeadsWithCoApplicant(
                applicantFirstName,
                applicantLastName,
                coApplicantFirstName,
                coApplicantLastName);
    }

    public void clickOnTheNewBtn() {
        leadsPageObject.clickOnTheNewBtn();
    }

    public void enterRequireFields(String lastName, String email) {
        leadsPageObject.enterRequireCustomerInfo(lastName, email);
    }

    public void clickSaveBtn() {
        leadsPageObject.clickOnSaveBtn();
    }

    public boolean isCustomerNameDisplayedOnLeadPage() {
        return leadsPageObject.isCustomerNameDisplayed();
    }

    public String getErrMsgLeadNotCreated() {
        if (leadsPageObject.isErrSnagDisplayed()) {
            return leadsPageObject.getErrSnagMsg();
        } else {
            return null;
        }
    }

    @Deprecated
    public boolean isLandedOnConvertTab() {
        leadsPageObject.clickOnConvertDropDownBtn();
        leadsPageObject.clickOnConvertBtn();
        return leadsPageObject.isConvertTabDisplayed();
    }

    @Deprecated
    public String requiredFieldInTheConvert() {
        return leadsPageObject.getTextLastNameOfConvertTab();
    }

    @Deprecated
    public void clearLastNameOnConvertTab() {
        leadsPageObject.clearLastNameOnConvertTab();
    }

    @Deprecated
    public void convertLeadToOpportunity() {
        leadsPageObject.clickOnConvertOpportunityBtn();
    }

    @Deprecated
    public String opportunityCreatedMsg() {
        return leadsPageObject.getSuccessMsgOpportunityConvert();
    }

    @Deprecated
    public void closeOpportunity() { leadsPageObject.closeOpportunityWindow();}

    public void clickManagerConversion() {
        logger.info("Click On the Manager Conversion Button");
        leadsPageObject.clickOnTheManagerConversionBtn();
    }

    public boolean managerConversion(boolean isPhoneNumberNeedInManagerConversion) {
        return commonProcedural.managerConversion(isPhoneNumberNeedInManagerConversion);
    }

    public String getOpportunityTitleName() {
        return leadsPageObject.getOpportunityTitleName();
    }

    public boolean setSalesAgentLeadOwner() {
        String salesAgent = BaseClass.config.getProperty("salesAgentName");
        return commonProcedural.setLeadOwner(
                Utils.splitString(salesAgent, " ")[0],
                Utils.splitString(salesAgent, " ")[1]).contains(salesAgent);
    }

    public List<String> getDuplicateLeadsName() { return leadPageProcedural.getDuplicateLeads(); }

    public String getCreatedTask(String taskName) {
        return leadPageProcedural.getCreatedTasks()
                .stream()
                .filter(task -> task.equals(taskName))
                .findFirst()
                .orElse(null);
    }

    public boolean isInContractFileCreated() { return leadPageProcedural.getInContractFileName() != null; }
}