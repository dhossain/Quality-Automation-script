package com.coa.qa.pages;

import com.coa.qa.pages.page.procedural.model.CommonProcedural;

public class CommonPageAction {
    private CommonProcedural commonProcedural = new CommonProcedural();

    public boolean IsCallScriptPopulated(String state) { return commonProcedural.callScriptTaskValidation(state); }

    public String getProcessor(String state) { return commonProcedural.getCoaProcessor(state); }
}
