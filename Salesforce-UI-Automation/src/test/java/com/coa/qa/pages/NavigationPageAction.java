package com.coa.qa.pages;

import com.coa.qa.pages.page.procedural.model.CommonProcedural;
import com.coa.qa.pages.pageobject.NavigationPageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class NavigationPageAction {
    private static final Logger logger = LogManager.getLogger(NavigationPageAction.class);
    private NavigationPageObject navigationPageObject = new NavigationPageObject();
    private CommonProcedural commonProcedural = new CommonProcedural();

    public void navigateToLeadPage() { commonProcedural.navigateToLeadPage(); }

    public boolean isLeadBtnDisplayed() {
        return navigationPageObject.isLeadsBtnDisplayed();
    }

    public boolean isLoggedOut() { return commonProcedural.loggedOut(); }

    public void closeOpenedTabs() {
        commonProcedural.closeTabs();
    }
}
