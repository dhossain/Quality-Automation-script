package com.coa.qa.restapi;

import com.clearone.commonUtils.GenerateTestData;
import com.coa.qa.commons.ApiUtils;
import com.coa.qa.commons.MarketingVendor;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;

public class LeadCreateAPI extends ApiProperties {
    private static final Logger logger = LogManager.getLogger(LeadCreateAPI.class);
    private CreateLeadModel createLeadModel = new CreateLeadModel();
    private ApiUtils apiUtils = new ApiUtils();

    private ResponseEntity leadCreateUsingApi(String firstName, String lastName) {
        logger.info("Creating Lead Using Lead Create API");
        try {
            String jsonBody = GenerateTestData.createLeadJson(
                    super.CREATE_LEAD_JSON,
                    firstName,
                    lastName,
                    MarketingVendor.TestMarketingVendorId.VALUE,
                    15000.00,
                    config.getProperty("salesAgentId")).toString();
            return createLeadModel.createLeadRestTemplate(super.CREATE_LEAD_URL, jsonBody);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Response leadCreateRestAssured(String firstName, String lastName) {
        logger.info("Creating Lead Using Lead Create API with Rest-Assured");
        try {
            String jsonBody = GenerateTestData.createLeadJson(
                    super.CREATE_LEAD_JSON,
                    firstName,
                    lastName,
                    MarketingVendor.TestMarketingVendorId.VALUE,
                    15000.00,
                    config.getProperty("salesAgentId")).toString();
            return createLeadModel.createLead(super.CREATE_LEAD_URL, jsonBody);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String  leadCreateWithRestAssured(String firstName, String lastName) throws ParseException {
        Response response = leadCreateRestAssured(firstName, lastName);
        Assert.assertEquals(response.statusCode(), 200,
                "Lead Is NOT created Through API");
        return apiUtils.getLeadUrl(response.getBody().asString());
    }

    public String  leadCreateWithRestTemplate(String firstName, String lastName) throws ParseException {
        ResponseEntity response = leadCreateUsingApi(firstName, lastName);
        Assert.assertEquals(response.getStatusCodeValue(), 200,
                "Lead Is NOT created Through API");
        return apiUtils.getLeadUrl(response.getBody());
    }

    public Response leadCreateWithRestAssured(String jsonBody) {
        return createLeadModel.createLead(super.CREATE_LEAD_URL, jsonBody);
    }
}
